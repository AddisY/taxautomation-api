-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2017 at 08:45 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tax_calculation`
--

-- --------------------------------------------------------

--
-- Table structure for table `allowances`
--

CREATE TABLE IF NOT EXISTS `allowances` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `employe_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_amount` double NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `allowances`
--

INSERT INTO `allowances` (`id`, `employe_id`, `type`, `total_amount`, `deleted_at`, `created_at`, `updated_at`) VALUES
('', '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', 'type2', 2332, NULL, '2017-02-20 12:10:15', '2017-02-20 12:10:15'),
('87e60880-f74c-11e6-b399-818bcabd8f3c', '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', 'type3', 2332, NULL, '2017-02-20 12:11:15', '2017-02-21 11:49:29'),
('dc55bf30-f762-11e6-84f7-a97bb8ef53b3', '707e1990-f5a7-11e6-bba0-35f50cafdd49', 'type4', 2332, NULL, '2017-02-20 14:51:05', '2017-02-21 11:49:29'),
('dc5ee910-f762-11e6-8390-c7001df46c1c', '707e1990-f5a7-11e6-bba0-35f50cafdd49', 'type1', 2332, NULL, '2017-02-20 14:51:05', '2017-02-20 14:51:05');

-- --------------------------------------------------------

--
-- Table structure for table `bonuses`
--

CREATE TABLE IF NOT EXISTS `bonuses` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `salary_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leave_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employe_informations`
--

CREATE TABLE IF NOT EXISTS `employe_informations` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `tin_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `cost_sharing` double NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employe_informations`
--

INSERT INTO `employe_informations` (`id`, `full_name`, `company_id`, `tin_number`, `department`, `start_date`, `cost_sharing`, `deleted_at`, `created_at`, `updated_at`, `status`) VALUES
('70674710-f5a7-11e6-aed2-1fa6d0d5cc84', 'abe1', 1, '564454656464', 'accounting', '2014-01-02', 5555, NULL, '2017-02-18 09:56:57', '2017-02-24 09:59:52', 0),
('707e1990-f5a7-11e6-bba0-35f50cafdd49', 'abeba', 1, '564454656464', 'accounting', '2015-01-02', 5555, NULL, '2017-02-18 09:56:57', '2017-02-18 09:56:57', 0),
('fc316450-f5a8-11e6-94b2-8d120e0007c6', 'abe1', 1, '564454656464', 'accounting', '2013-01-02', 5555, NULL, '2017-02-18 10:08:01', '2017-02-18 10:08:01', 0),
('fc4ca0a0-f5a8-11e6-a377-835829c327a0', 'abel3', 1, '564454656464', 'accounting', '2011-01-02', 5555, NULL, '2017-02-18 10:08:01', '2017-02-18 10:08:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `encashed_leaves`
--

CREATE TABLE IF NOT EXISTS `encashed_leaves` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `employe_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `annual_leave` int(11) NOT NULL,
  `leave_date` int(11) NOT NULL,
  `working_date` int(11) NOT NULL,
  `served_leaved_period` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leave_accumulated` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `encashed_leaves`
--

INSERT INTO `encashed_leaves` (`id`, `employe_id`, `annual_leave`, `leave_date`, `working_date`, `served_leaved_period`, `leave_accumulated`, `deleted_at`, `created_at`, `updated_at`) VALUES
('0f704410-fcbf-11e6-87b1-0b946b4a02cf', '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', 14, 10, 250, '3-2', '10', NULL, '2017-02-27 10:33:40', '2017-02-27 10:33:40'),
('0f7b38b0-fcbf-11e6-902d-83b84e045790', '707e1990-f5a7-11e6-bba0-35f50cafdd49', 14, 10, 250, '3-2', '10', NULL, '2017-02-27 10:33:41', '2017-02-27 10:33:41'),
('0f8392e0-fcbf-11e6-a56d-277b67ff5601', 'fc316450-f5a8-11e6-94b2-8d120e0007c6', 14, 10, 250, '3-2', '10', NULL, '2017-02-27 10:33:41', '2017-02-27 10:33:41');

-- --------------------------------------------------------

--
-- Table structure for table `foreign_purchases`
--

CREATE TABLE IF NOT EXISTS `foreign_purchases` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dec_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `dec_date` date NOT NULL,
  `bill_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax_price` double NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `foreign_purchases`
--

INSERT INTO `foreign_purchases` (`id`, `dec_number`, `company_id`, `dec_date`, `bill_code`, `tax_price`, `item_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
('585d1350-f766-11e6-aed5-bfd045d335ae', '11111111111111111111111', 1, '2016-12-11', '1185222', 232, 'dell', NULL, '2017-02-20 15:16:02', '2017-02-20 15:16:02'),
('586eda40-f766-11e6-8c6f-83528995f6e6', '222222222222', 1, '2016-12-11', '1185222', 232, 'dell', NULL, '2017-02-20 15:16:02', '2017-02-20 15:16:02'),
('5bb85fc0-f43a-11e6-9876-271b46e4fe39', '11111111111111111111111', 1, '2016-12-11', '1185222', 232, 'dell', NULL, '2017-02-16 14:23:36', '2017-02-16 14:23:36'),
('5bcd2980-f43a-11e6-b327-597e4727232a', '222222222222', 1, '2016-12-11', '1185222', 232, 'dell', NULL, '2017-02-16 14:23:36', '2017-02-16 14:23:36'),
('b35c2f00-f420-11e6-80e0-b98dc4c61c79', '233224354543243', 1, '2016-12-11', '1185222', 232, 'dell', NULL, '2017-02-16 11:19:56', '2017-02-16 11:19:56');

-- --------------------------------------------------------

--
-- Table structure for table `local_purchases`
--

CREATE TABLE IF NOT EXISTS `local_purchases` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `item_price` double NOT NULL,
  `tin_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `withholding_tax_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `purchase_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mrc_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bill_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bill_date` date NOT NULL,
  `withholding_date` date NOT NULL,
  `withholding_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `local_purchases`
--

INSERT INTO `local_purchases` (`id`, `company_name`, `company_id`, `item_price`, `tin_number`, `item_type`, `withholding_tax_number`, `purchase_type`, `mrc_code`, `bill_code`, `item_name`, `bill_date`, `withholding_date`, `withholding_code`, `deleted_at`, `created_at`, `updated_at`) VALUES
('671898a0-f427-11e6-8715-2faa90f3ec1a', 'yokida', 1, 1000, '233224354543243', 'local;', '855544888', 'product', '4546112377', '32423423432324', 'food', '2016-12-11', '2016-12-11', '1185222', NULL, '2017-02-16 12:07:55', '2017-02-16 12:07:55'),
('be2a2200-f436-11e6-a13b-354c8a51e3f9', 'company3', 1, 1000, '233224354543243', 'local;', '855544888', 'product', '4546112377', '32423423432324', 'food', '2016-12-11', '2016-12-11', '1185222', NULL, '2017-02-16 13:57:43', '2017-02-16 13:57:43'),
('be36a860-f436-11e6-a400-e7c507c59785', 'company3', 1, 1000, '233224354543243', 'local;', '855544888', 'product', '4546112377', '32423423432324', 'food', '2016-12-11', '2016-12-11', '1185222', NULL, '2017-02-16 13:57:43', '2017-02-16 13:57:43'),
('c2017100-f436-11e6-94f9-7d2abc93ddfb', 'company3', 1, 1000, '233224354543243', 'local;', '855544888', 'product', '4546112377', '32423423432324', 'food', '2016-12-11', '2016-12-11', '1185222', NULL, '2017-02-16 13:57:50', '2017-02-16 13:57:50'),
('c2072df0-f436-11e6-8e61-7f4a3973415a', 'company3', 1, 1000, '233224354543243', 'local;', '855544888', 'product', '4546112377', '32423423432324', 'food', '2016-12-11', '2016-12-11', '1185222', NULL, '2017-02-16 13:57:50', '2017-02-16 13:57:50'),
('c4cf1d20-f436-11e6-bd6d-3308a847f02d', 'company3', 1, 1000, '233224354543243', 'local;', '855544888', 'product', '4546112377', '32423423432324', 'food', '2016-12-11', '2016-12-11', '1185222', NULL, '2017-02-16 13:57:54', '2017-02-16 13:57:54'),
('c4d60370-f436-11e6-8fbe-930d4d529ad7', 'company3', 1, 1000, '233224354543243', 'local;', '855544888', 'product', '4546112377', '32423423432324', 'food', '2016-12-11', '2016-12-11', '1185222', NULL, '2017-02-16 13:57:54', '2017-02-16 13:57:54'),
('cf0631e0-f435-11e6-870c-d1c081ce34cf', 'company1', 1, 1000, '233224354543243', 'local;', '855544888', 'product', '4546112377', '32423423432324', 'food', '2016-12-11', '2016-12-11', '1185222', NULL, '2017-02-16 13:51:02', '2017-02-16 13:51:02'),
('cf10c040-f435-11e6-a5d6-b7efbdb76e0f', 'company2', 1, 1000, '233224354543243', 'local;', '855544888', 'product', '4546112377', '32423423432324', 'food', '2016-12-11', '2016-12-11', '1185222', NULL, '2017-02-16 13:51:02', '2017-02-16 13:51:02'),
('fc274190-f436-11e6-bf67-e5aafab721ec', 'company5', 1, 1000, '233224354543243', 'local;', '855544888', 'product', '4546112377', '32423423432324', 'food', '2016-12-11', '2016-12-11', '1185222', NULL, '2017-02-16 13:59:27', '2017-02-16 13:59:27'),
('fc345dc0-f436-11e6-aeab-db996dfa6bff', 'company6', 1, 1000, '233224354543243', 'local;', '855544888', 'product', '4546112377', '32423423432324', 'food', '2016-12-11', '2016-12-11', '1185222', NULL, '2017-02-16 13:59:27', '2017-02-16 13:59:27');

-- --------------------------------------------------------

--
-- Table structure for table `local_sales`
--

CREATE TABLE IF NOT EXISTS `local_sales` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bill_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bill_date` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `local_sales`
--

INSERT INTO `local_sales` (`id`, `company_id`, `item_name`, `item_price`, `bill_code`, `bill_date`, `deleted_at`, `created_at`, `updated_at`) VALUES
('b58ab960-f43a-11e6-b732-8d62c6a8527f', 1, 'food', '55', '132134234', '2016-11-18', NULL, '2017-02-16 14:26:07', '2017-02-16 14:26:07'),
('b596ec50-f43a-11e6-805d-9308b10ece9e', 1, 'food2', '55', '132134234', '2016-11-18', NULL, '2017-02-16 14:26:07', '2017-02-16 14:26:07'),
('b9e63e60-f4eb-11e6-aef0-53a5fd92752f', 1, 'food4', '55', '132134234', '2016-11-18', NULL, '2017-02-17 11:33:15', '2017-02-17 11:33:15'),
('b9f05bc0-f4eb-11e6-a3e0-cf1a8febfa49', 1, 'food5', '55', '132134234', '2016-11-18', NULL, '2017-02-17 11:33:15', '2017-02-17 11:33:15'),
('cf6ef6a0-f4eb-11e6-bab1-d7a1ce62bc9f', 1, 'food4', '55', '132134234', '2016-11-18', NULL, '2017-02-17 11:33:51', '2017-02-17 11:33:51'),
('cf7a0960-f4eb-11e6-bac9-b38afc559b88', 1, 'food5', '55', '132134234', '2016-11-18', NULL, '2017-02-17 11:33:51', '2017-02-17 11:33:51');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_04_24_110151_create_oauth_scopes_table', 1),
('2014_04_24_110304_create_oauth_grants_table', 1),
('2014_04_24_110403_create_oauth_grant_scopes_table', 1),
('2014_04_24_110459_create_oauth_clients_table', 1),
('2014_04_24_110557_create_oauth_client_endpoints_table', 1),
('2014_04_24_110705_create_oauth_client_scopes_table', 1),
('2014_04_24_110817_create_oauth_client_grants_table', 1),
('2014_04_24_111002_create_oauth_sessions_table', 1),
('2014_04_24_111109_create_oauth_session_scopes_table', 1),
('2014_04_24_111254_create_oauth_auth_codes_table', 1),
('2014_04_24_111403_create_oauth_auth_code_scopes_table', 1),
('2014_04_24_111518_create_oauth_access_tokens_table', 1),
('2014_04_24_111657_create_oauth_access_token_scopes_table', 1),
('2014_04_24_111810_create_oauth_refresh_tokens_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_02_24_202335_entrust_setup_tables', 1),
('2017_02_11_012317_create_local_purchase_table', 1),
('2017_02_11_012423_create_foreign_purchase_table', 1),
('2017_02_11_012507_create_local_sales_table', 1),
('2017_02_11_024535_create_non-registered_purchases_table', 1),
('2017_02_15_072020_create_employeinformations_table', 1),
('2017_02_15_073800_create_officeexpenses_table', 1),
('2017_02_15_074615_create_encashedleaves_table', 1),
('2017_02_15_082830_create_overtimes_table', 1),
('2017_02_16_002223_create_salaries_table', 1),
('2017_02_16_002929_create_terminations_table', 1),
('2017_02_16_003654_create_allowances_table', 1),
('2017_02_16_003919_create_bonuses_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nonregistered_purchases`
--

CREATE TABLE IF NOT EXISTS `nonregistered_purchases` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `zone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `woreda` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `house_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `purchase_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_price` double NOT NULL,
  `voucher_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `voucher_date` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nonregistered_purchases`
--

INSERT INTO `nonregistered_purchases` (`id`, `company_name`, `company_id`, `zone`, `sub_city`, `woreda`, `house_number`, `purchase_type`, `item_price`, `voucher_number`, `voucher_date`, `deleted_at`, `created_at`, `updated_at`) VALUES
('dd7245a0-f43b-11e6-a568-d73247e96dda', 'company1', 1, '03', 'bole', '18', '145', 'service', 55, '555887', '2016-12-11', NULL, '2017-02-16 14:34:23', '2017-02-16 14:34:23'),
('dd83f3b0-f43b-11e6-a493-b9bc08416296', 'company2', 1, '03', 'bole', '18', '145', 'service', 55, '555887', '2016-12-11', NULL, '2017-02-16 14:34:23', '2017-02-16 14:34:23');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `session_id`, `expire_time`, `created_at`, `updated_at`) VALUES
('3RNYiSyP70CtiRdcqTyTkeDiQ5d6mz3W24wUYfbu', 33, 1487930500, '2017-02-24 12:01:41', '2017-02-24 12:01:41'),
('5o8nmUDFJONCuaekR6Knrz1YBYQkUO3DzjVk60je', 25, 1487840693, '2017-02-23 11:04:54', '2017-02-23 11:04:54'),
('5qvZAjYHkuFCky0U826O14nvqKJWj09MvXffeuLt', 41, 1488183499, '2017-02-27 10:18:19', '2017-02-27 10:18:19'),
('8E5hoO0wANhmYn3Q1acTI8SzXoftMoNDiEE5SrFM', 22, 1487750490, '2017-02-22 10:01:30', '2017-02-22 10:01:30'),
('91sdqnhAAs1HwpDp1dhIuBDnxMe8vs2z9lNQjqlB', 36, 1487947653, '2017-02-24 16:47:33', '2017-02-24 16:47:33'),
('955sNCNZs23C3bYolkS6yFSdtOiekpeDcKylaxrx', 13, 1487593546, '2017-02-20 14:25:47', '2017-02-20 14:25:47'),
('9H7WrGdVRhlosDCOUIqENubDennODf6Zc0XO4gS2', 4, 1487252067, '2017-02-16 15:34:27', '2017-02-16 15:34:27'),
('AlOQZW3krH5oaT7VUbYVQnQgf1kOhyE7yEV7kpX2', 8, 1487402933, '2017-02-18 09:28:53', '2017-02-18 09:28:53'),
('aWWJo2n0roQwKKcFhrawG6EANExFLLLVQZWFexg8', 32, 1487926780, '2017-02-24 10:59:41', '2017-02-24 10:59:41'),
('B9FMo8fQELlMfexbAbLiYAoK5AnCSEOAScQ4YXXc', 20, 1487687125, '2017-02-21 16:25:25', '2017-02-21 16:25:25'),
('civsTxHhWfylkr2EuqRN9kwWVoQ7namPM7qVyqRP', 31, 1487926311, '2017-02-24 10:51:51', '2017-02-24 10:51:51'),
('cmVtXqmpVRcRWxmxQu3AMRE4QidMo7jZPKFJL0yu', 6, 1487254314, '2017-02-16 16:11:54', '2017-02-16 16:11:54'),
('dzr7zhSLarUQt7oMqJm0Km5x6t8tIQtyS6dOeklG', 28, 1487857271, '2017-02-23 15:41:11', '2017-02-23 15:41:11'),
('ElcggKfPbO8BtcmQiHYSRAr9sgBCmJD3XWLg1DCt', 43, 1488200788, '2017-02-27 15:06:28', '2017-02-27 15:06:28'),
('FPfFa1a11BBcnjktNDsUFGRplUYFdkqcvnwEEzyx', 26, 1487845213, '2017-02-23 12:20:13', '2017-02-23 12:20:13'),
('gAA35cVXL8IH4QUL5up8BxH6KpAbIKcZGkvhxXal', 23, 1487765941, '2017-02-22 14:19:01', '2017-02-22 14:19:01'),
('HNqyboPEx32Nm9QytEfn98tJBQXU0wFSrpQA06fz', 9, 1487407459, '2017-02-18 10:44:19', '2017-02-18 10:44:19'),
('Iru7VEbFfxPLqLhHfMpSD5nm6DB7cSNIO61pDbiy', 24, 1487836569, '2017-02-23 09:56:10', '2017-02-23 09:56:10'),
('JdcVi4iX7Hiytfq3SAjtOAzDJfmKUvIpWjVsc59n', 18, 1487679021, '2017-02-21 14:10:21', '2017-02-21 14:10:21'),
('jggOpAkOizZIRfqvpRkAuTUXQcJreDcEZtKw30on', 27, 1487853646, '2017-02-23 14:40:46', '2017-02-23 14:40:46'),
('JVEMOxRli7eC0RqBAM2DYhPAsQ1Y9uYgSbSkRuXs', 17, 1487667452, '2017-02-21 10:57:32', '2017-02-21 10:57:32'),
('kFaJRy5ZwcCFNjB4XOhLeDElnOTyByE4Gmb4kpq3', 21, 1487746876, '2017-02-22 09:01:16', '2017-02-22 09:01:16'),
('kLgSHJk4wG7ytuKdeh3F0AFr7Iz0lE82jkidxOP4', 5, 1487254262, '2017-02-16 16:11:02', '2017-02-16 16:11:02'),
('L2vitLxWlPo4hg4Oa29gWGfhkPWvUZSCsQJBXwtQ', 12, 1487585343, '2017-02-20 12:09:03', '2017-02-20 12:09:03'),
('MANXNe1bjEW2QgP1usgPyC6IfhsbCszWwGxc5qUd', 7, 1487323937, '2017-02-17 11:32:17', '2017-02-17 11:32:17'),
('MEWZZOPmJkiJCpcCJ92NMu6EpJX0utkWxLCqTjYz', 11, 1487581480, '2017-02-20 11:04:40', '2017-02-20 11:04:40'),
('NkYNlwbSVCgOHNkt8EnlaIzFa9wVvu9m6qRW786c', 45, 1488266472, '2017-02-28 09:21:12', '2017-02-28 09:21:12'),
('ow2A8fBRKOTqUfKsyH5lnB4KVTwkUi2FpYjTTDKY', 2, 1487245839, '2017-02-16 13:50:39', '2017-02-16 13:50:39'),
('pdomUZquIdLTilL40H0OcINbJ1bPhUNBQq167t18', 34, 1487940089, '2017-02-24 14:41:29', '2017-02-24 14:41:29'),
('PMv7GCEfiBrjgvqCr1mDjp2FNGVx8hy5RohEp2h3', 39, 1488017762, '2017-02-25 12:16:02', '2017-02-25 12:16:02'),
('QJRouqlX90FwhiKcdbAsq4dOfQnJJHlXBbau5wFD', 35, 1487943764, '2017-02-24 15:42:44', '2017-02-24 15:42:44'),
('QpMQTX84DoEsq45hHBnvM4qekCPt4viY8TEhLjY2', 19, 1487683481, '2017-02-21 15:24:41', '2017-02-21 15:24:41'),
('RyVoWDEsrRMJ5XZLdZFrqEUV3K1dLkFoonS8VFWI', 29, 1487862678, '2017-02-23 17:11:18', '2017-02-23 17:11:18'),
('s0REclySLd38TQpG4Jcza5OXMq2N78D1Xe2luRlf', 42, 1488189417, '2017-02-27 11:56:57', '2017-02-27 11:56:57'),
('uFpWtj3aMVRNV7V72XI4WGJgz49N6ifNGxJTD2Mm', 44, 1488203904, '2017-02-27 15:58:24', '2017-02-27 15:58:24'),
('uppg33VNjj5l8zzoKXLpWqp1N0dTjQKiWJSCnovy', 37, 1487970718, '2017-02-24 23:11:58', '2017-02-24 23:11:58'),
('uq7Yo0zH24KWTNaw5O2myBrPLUdbFCLrm87kih0s', 3, 1487248086, '2017-02-16 14:28:06', '2017-02-16 14:28:06'),
('VWuCcidMdel4qhLTF5ur076zM3QBZZjGPA1OqiLT', 1, 1487236199, '2017-02-16 11:09:59', '2017-02-16 11:09:59'),
('W4Uiz4p1VOsHvtTqAj4blebK1S6eONJtWx5JH7YX', 16, 1487661335, '2017-02-21 09:15:36', '2017-02-21 09:15:36'),
('XMG543NptsODb2VjshDmHQtNSotujvXDw7jeGSrH', 10, 1487577761, '2017-02-20 10:02:42', '2017-02-20 10:02:42'),
('YcQpgLA8ZivqCOnewuFmEOWVnKtJpgmAKufWxtfO', 40, 1488177429, '2017-02-27 08:37:09', '2017-02-27 08:37:09'),
('yRULUvm1Gtl3Ydf17t07qNr4lrqzfmYUN0IlC1uN', 14, 1487594972, '2017-02-20 14:49:32', '2017-02-20 14:49:32'),
('z0ocLfLFRXtGn8p7NLOeAoWiJQeHutlWVNElm8rB', 30, 1487922648, '2017-02-24 09:50:49', '2017-02-24 09:50:49'),
('ZIAUiih5Q4uz5zVANIdG4sWmaupiMhZ3CZDRBlL2', 38, 1488005580, '2017-02-25 08:53:00', '2017-02-25 08:53:00'),
('zpfEsIHdNDh2gP3GYFbFOzHW3EWaFjVP3hJVDxuO', 15, 1487598605, '2017-02-20 15:50:05', '2017-02-20 15:50:05');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_token_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_access_token_scopes` (
  `id` int(10) unsigned NOT NULL,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_code_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_auth_code_scopes` (
  `id` int(10) unsigned NOT NULL,
  `auth_code_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `secret`, `name`, `created_at`, `updated_at`) VALUES
('$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', '$2y$10$9OqJjxC9qZKAB2L.nO7hVOPY0436eU1CD', 'Web Client', '2017-02-16 10:52:04', '2017-02-16 10:52:04');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_client_endpoints`
--

CREATE TABLE IF NOT EXISTS `oauth_client_endpoints` (
  `id` int(10) unsigned NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_client_grants`
--

CREATE TABLE IF NOT EXISTS `oauth_client_grants` (
  `id` int(10) unsigned NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_client_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_client_scopes` (
  `id` int(10) unsigned NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_grants`
--

CREATE TABLE IF NOT EXISTS `oauth_grants` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_grant_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_grant_scopes` (
  `id` int(10) unsigned NOT NULL,
  `grant_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_scopes` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_sessions`
--

CREATE TABLE IF NOT EXISTS `oauth_sessions` (
  `id` int(10) unsigned NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `owner_type` enum('client','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `owner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_redirect_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_sessions`
--

INSERT INTO `oauth_sessions` (`id`, `client_id`, `owner_type`, `owner_id`, `client_redirect_uri`, `created_at`, `updated_at`) VALUES
(1, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-16 11:09:59', '2017-02-16 11:09:59'),
(2, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-16 13:50:39', '2017-02-16 13:50:39'),
(3, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-16 14:28:06', '2017-02-16 14:28:06'),
(4, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-16 15:34:27', '2017-02-16 15:34:27'),
(5, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-16 16:11:02', '2017-02-16 16:11:02'),
(6, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-16 16:11:54', '2017-02-16 16:11:54'),
(7, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-17 11:32:17', '2017-02-17 11:32:17'),
(8, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-18 09:28:53', '2017-02-18 09:28:53'),
(9, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-18 10:44:19', '2017-02-18 10:44:19'),
(10, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-20 10:02:41', '2017-02-20 10:02:41'),
(11, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-20 11:04:40', '2017-02-20 11:04:40'),
(12, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-20 12:09:03', '2017-02-20 12:09:03'),
(13, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-20 14:25:46', '2017-02-20 14:25:46'),
(14, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-20 14:49:32', '2017-02-20 14:49:32'),
(15, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-20 15:50:05', '2017-02-20 15:50:05'),
(16, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-21 09:15:36', '2017-02-21 09:15:36'),
(17, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-21 10:57:32', '2017-02-21 10:57:32'),
(18, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-21 14:10:21', '2017-02-21 14:10:21'),
(19, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-21 15:24:41', '2017-02-21 15:24:41'),
(20, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-21 16:25:25', '2017-02-21 16:25:25'),
(21, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-22 09:01:16', '2017-02-22 09:01:16'),
(22, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-22 10:01:30', '2017-02-22 10:01:30'),
(23, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-22 14:19:01', '2017-02-22 14:19:01'),
(24, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-23 09:56:09', '2017-02-23 09:56:09'),
(25, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-23 11:04:53', '2017-02-23 11:04:53'),
(26, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-23 12:20:13', '2017-02-23 12:20:13'),
(27, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-23 14:40:46', '2017-02-23 14:40:46'),
(28, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-23 15:41:11', '2017-02-23 15:41:11'),
(29, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-23 17:11:18', '2017-02-23 17:11:18'),
(30, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-24 09:50:49', '2017-02-24 09:50:49'),
(31, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-24 10:51:51', '2017-02-24 10:51:51'),
(32, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-24 10:59:40', '2017-02-24 10:59:40'),
(33, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-24 12:01:40', '2017-02-24 12:01:40'),
(34, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-24 14:41:29', '2017-02-24 14:41:29'),
(35, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-24 15:42:44', '2017-02-24 15:42:44'),
(36, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-24 16:47:33', '2017-02-24 16:47:33'),
(37, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-24 23:11:58', '2017-02-24 23:11:58'),
(38, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-25 08:53:00', '2017-02-25 08:53:00'),
(39, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-25 12:16:02', '2017-02-25 12:16:02'),
(40, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-27 08:37:09', '2017-02-27 08:37:09'),
(41, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-27 10:18:19', '2017-02-27 10:18:19'),
(42, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-27 11:56:57', '2017-02-27 11:56:57'),
(43, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-27 15:06:28', '2017-02-27 15:06:28'),
(44, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-27 15:58:24', '2017-02-27 15:58:24'),
(45, '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', 'client', '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD', NULL, '2017-02-28 09:21:12', '2017-02-28 09:21:12');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_session_scopes` (
  `id` int(10) unsigned NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `scope_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `office_expenses`
--

CREATE TABLE IF NOT EXISTS `office_expenses` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `issue_date` date NOT NULL,
  `bill_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_price` double NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `office_expenses`
--

INSERT INTO `office_expenses` (`id`, `issue_date`, `bill_code`, `total_price`, `company_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
('dfb141e0-f73a-11e6-b35a-efc9b7d4ded7', '2016-10-11', '32434234', 1000, 1, NULL, '2017-02-20 10:04:51', '2017-02-20 10:04:51');

-- --------------------------------------------------------

--
-- Table structure for table `overtimes`
--

CREATE TABLE IF NOT EXISTS `overtimes` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `employe_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `overtime_date` date NOT NULL,
  `start_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `end_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `overtimes`
--

INSERT INTO `overtimes` (`id`, `employe_id`, `overtime_date`, `start_time`, `end_time`, `deleted_at`, `created_at`, `updated_at`) VALUES
('84c034e0-f8c9-11e6-bf7d-6f37dbc6f20c', '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', '2016-10-11', '1:00', '4:30', NULL, '2017-02-22 09:38:28', '2017-02-22 09:38:28'),
('84d007d0-f8c9-11e6-a706-27b00450740b', '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', '2016-10-12', '4:00', '4:30', NULL, '2017-02-22 09:38:28', '2017-02-22 09:38:28'),
('a959f360-f8cb-11e6-9ce6-6f7610c6e9e9', '707e1990-f5a7-11e6-bba0-35f50cafdd49', '2016-10-11', '1:00', '4:30', NULL, '2017-02-22 09:53:48', '2017-02-22 09:53:48'),
('a963fa20-f8cb-11e6-9494-f344f1921746', '707e1990-f5a7-11e6-bba0-35f50cafdd49', '2016-10-12', '4:00', '4:30', NULL, '2017-02-22 09:53:48', '2017-02-22 09:53:48'),
('b1b77620-f8c9-11e6-8720-7760948d618b', '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', '2016-10-11', '1:00', '4:30', NULL, '2017-02-22 09:39:43', '2017-02-22 09:39:43'),
('b1c4bd30-f8c9-11e6-a5b6-85573b0845ae', '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', '2016-10-12', '4:00', '4:30', NULL, '2017-02-22 09:39:43', '2017-02-22 09:39:43'),
('fe8a5320-f73c-11e6-a2e3-c3f70218e530', '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', '2016-10-12', '2:00', '4:30', NULL, '2017-02-20 10:20:02', '2017-02-20 10:20:02');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salaries`
--

CREATE TABLE IF NOT EXISTS `salaries` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total_salary` double NOT NULL,
  `employe_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `salaries`
--

INSERT INTO `salaries` (`id`, `total_salary`, `employe_id`, `payment_type`, `duration`, `deleted_at`, `created_at`, `updated_at`) VALUES
('3703b790-fa68-11e6-97c5-8b32dea86ffe', 55544, '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', 'variable', 'month1', NULL, '2017-02-24 11:06:58', '2017-02-24 11:06:58'),
('7072cdf0-f5a7-11e6-a25b-d78ffa45fea9', 55544, '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', 'variable', 'month2', NULL, '2017-02-18 09:56:57', '2017-02-24 10:45:27'),
('708a02f0-f5a7-11e6-9b39-5515490682c0', 55544, '707e1990-f5a7-11e6-bba0-35f50cafdd49', 'fixed', '2016', NULL, '2017-02-18 09:56:57', '2017-02-18 09:56:57'),
('81eaecb0-fa68-11e6-b89e-fd6d1f4d167c', 55544, '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', 'variable', 'month1', NULL, '2017-02-24 11:09:04', '2017-02-24 11:09:04'),
('f5d2aab0-fa67-11e6-a4a4-e71bab8df454', 55544, '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', 'variable', 'month1', NULL, '2017-02-24 11:05:09', '2017-02-24 11:05:09'),
('fc3a7520-f5a8-11e6-a99a-c1ca3b382a32', 55544, 'fc316450-f5a8-11e6-94b2-8d120e0007c6', 'variable', 'month1', NULL, '2017-02-18 10:08:01', '2017-02-18 10:08:01');

-- --------------------------------------------------------

--
-- Table structure for table `terminations`
--

CREATE TABLE IF NOT EXISTS `terminations` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `employe_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `end_date` date NOT NULL,
  `type` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `terminations`
--

INSERT INTO `terminations` (`id`, `status`, `employe_id`, `end_date`, `type`, `deleted_at`, `created_at`, `updated_at`) VALUES
('c32f0d80-fccb-11e6-9fd6-5b0df93252ee', 0, '70674710-f5a7-11e6-aed2-1fa6d0d5cc84', '2016-08-02', 1, NULL, '2017-02-27 12:04:36', '2017-02-27 12:04:36'),
('c33644a0-fccb-11e6-ac35-fdd6e0a1c472', 0, '707e1990-f5a7-11e6-bba0-35f50cafdd49', '2017-01-02', 2, NULL, '2017-02-27 12:04:36', '2017-02-27 12:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(10) unsigned NOT NULL COMMENT 'Primary Key: Unique user ID.',
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'Unique user name.',
  `pass` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'User?s password (hashed).',
  `mail` varchar(254) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'User?s e-mail address.',
  `theme` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'User?s default theme.',
  `signature` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'User?s signature.',
  `signature_format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'The filter_format.format of the signature.',
  `created` int(11) NOT NULL DEFAULT '0' COMMENT 'Timestamp for when user was created.',
  `access` int(11) NOT NULL DEFAULT '0' COMMENT 'Timestamp for previous time user accessed the site.',
  `login` int(11) NOT NULL DEFAULT '0' COMMENT 'Timestamp for user?s last login.',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the user is active(1) or blocked(0).',
  `timezone` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'User?s time zone.',
  `language` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'User?s default language.',
  `picture` int(11) NOT NULL DEFAULT '0' COMMENT 'Foreign key: file_managed.fid of user?s picture.',
  `init` varchar(254) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'E-mail address used for initial account creation.',
  `data` blob COMMENT 'A serialized array of name value pairs that are related to the user. Any form values posted during user edit are stored and are loaded into the $user object during user_load(). Use of this field is discouraged and it will likely disappear in a future...',
  `first_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line_number` int(11) DEFAULT NULL,
  `mobile_number` int(11) DEFAULT NULL,
  `tin_number` int(11) DEFAULT NULL,
  `decoy` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `package` int(11) NOT NULL,
  `payment` int(11) DEFAULT NULL,
  `cheque_no` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank` text COLLATE utf8_unicode_ci,
  `branch` text COLLATE utf8_unicode_ci,
  `type` int(11) DEFAULT NULL,
  `construction` tinyint(1) DEFAULT NULL,
  `industry` tinyint(1) DEFAULT NULL,
  `service` tinyint(1) DEFAULT NULL,
  `ngo` tinyint(1) DEFAULT NULL,
  `agriculture` tinyint(1) DEFAULT NULL,
  `bank_insurance` tinyint(1) DEFAULT NULL,
  `import_export` tinyint(1) DEFAULT NULL,
  `other_sectors` tinyint(1) DEFAULT NULL,
  `business_entity` tinyint(1) DEFAULT NULL,
  `country` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `sub_city` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(60) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `name`, `pass`, `mail`, `theme`, `signature`, `signature_format`, `created`, `access`, `login`, `status`, `timezone`, `language`, `picture`, `init`, `data`, `first_name`, `middle_name`, `last_name`, `line_number`, `mobile_number`, `tin_number`, `decoy`, `package`, `payment`, `cheque_no`, `bank`, `branch`, `type`, `construction`, `industry`, `service`, `ngo`, `agriculture`, `bank_insurance`, `import_export`, `other_sectors`, `business_entity`, `country`, `city`, `sub_city`, `province`) VALUES
(1, 'admin', '$S$DfaUipaGkzpWRmzJrPxD1f7W2HMR8vfit6.N2Fytih/UTfcReruG', 'support@drupalexp.com', '', '', 'full_html', 1398700980, 1486744408, 1486669150, 1, 'Asia/Krasnoyarsk', 'en', 413, 'support@drupalexp.com', 0x613a363a7b733a31363a22636b656469746f725f64656661756c74223b733a313a2274223b733a32303a22636b656469746f725f73686f775f746f67676c65223b733a313a2274223b733a31343a22636b656469746f725f7769647468223b733a343a2231303025223b733a31333a22636b656469746f725f6c616e67223b733a323a22656e223b733a31383a22636b656469746f725f6175746f5f6c616e67223b733a313a2274223b733a373a22636f6e74616374223b693a313b7d, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', ''),
(98, 'admin2', '$S$DNsTBttTYWym6B3MaysY2Vn.9AFFBM4sDlQ8Nyqs8DJ5936bFmHF', 'bekigher@gmail.com', '', '', 'filtered_html', 1484311854, 1485603597, 1485522730, 1, 'Asia/Krasnoyarsk', 'en', 0, 'bekigher@gmail.com', 0x613a363a7b733a373a22636f6e74616374223b693a313b733a31363a22636b656469746f725f64656661756c74223b733a313a2274223b733a32303a22636b656469746f725f73686f775f746f67676c65223b733a313a2274223b733a31343a22636b656469746f725f7769647468223b733a343a2231303025223b733a31333a22636b656469746f725f6c616e67223b733a323a22656e223b733a31383a22636b656469746f725f6175746f5f6c616e67223b733a313a2274223b7d, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', ''),
(122, 'yonas', '$S$Dycv56ejKKubvysdFUfoCNDrg9lDUkEZWmMymLlFCh7xCVlBsqW5', 'yonas@yokidaconsult.com', '', '', 'full_html', 1485264844, 1486744515, 1486740130, 1, 'Asia/Krasnoyarsk', 'en', 0, 'yonas@yokidaconsult.com', 0x613a363a7b733a31363a22636b656469746f725f64656661756c74223b733a313a2274223b733a32303a22636b656469746f725f73686f775f746f67676c65223b733a313a2274223b733a31343a22636b656469746f725f7769647468223b733a343a2231303025223b733a31333a22636b656469746f725f6c616e67223b733a323a22656e223b733a31383a22636b656469746f725f6175746f5f6c616e67223b733a313a2274223b733a373a22636f6e74616374223b693a313b7d, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', ''),
(208, 'qq', '242Q7Beink', 'qq', '', '', NULL, 0, 1486625441, 0, 0, NULL, '', 0, 'qq', NULL, NULL, NULL, NULL, 44, 4, 55, 'qq', 4, 2, '', '1', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', ''),
(209, 'YYY', 'HhYcaLjZrr', 'TTT@SAD', '', '', NULL, 0, 1486628726, 0, 0, NULL, '', 0, 'TTT@SAD', NULL, NULL, NULL, NULL, 7656, 345, 235, 'YYY', 5, 2, '', '1', NULL, 2, NULL, 1, 2, 0, 0, 0, 0, 0, NULL, '', '', '', ''),
(210, 'JJJJ', 'G3KezSouUz', 'JJJJJJ', '', '', NULL, 0, 1486628937, 0, 0, NULL, '', 0, 'JJJJJJ', NULL, NULL, NULL, NULL, 45645, 5765, 234523, 'JJJ', 5, 2, '', '1', NULL, 2, 0, 0, 0, 3, 4, 5, 0, 0, NULL, '', '', '', ''),
(211, 'qwer', 'mHsPsB4eJ7', 'fas@asfs', '', '', NULL, 0, 1486629640, 0, 0, NULL, '', 0, 'fas@asfs', NULL, NULL, NULL, NULL, 24352345, 555, 455, 'qwrwqer', 6, 2, '', '1', NULL, 2, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, '', '', '', ''),
(212, 'uuuu', 'tZxyDXV7AS', 'uuuu', '', '', NULL, 0, 1486737616, 0, 0, NULL, '', 0, 'uuuu', NULL, NULL, NULL, NULL, 234, 23434, 23432, 'uuuu', 4, 2, '', '1', NULL, 2, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 'AF', 'uuuu', 'uuuu', 'uuuu'),
(213, 'yyyy', 'rBPGNbZd2n', 'yyyy', '', '', NULL, 0, 1486739788, 0, 0, NULL, '', 0, 'yyyy', NULL, NULL, NULL, NULL, 465645, 456456, 565, 'yyyy', 4, 2, '', '1', NULL, 2, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 'AF', 'yyyy', 'yyyy', 'yyyy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allowances`
--
ALTER TABLE `allowances`
  ADD PRIMARY KEY (`id`), ADD KEY `allowances_employe_id_foreign` (`employe_id`);

--
-- Indexes for table `bonuses`
--
ALTER TABLE `bonuses`
  ADD PRIMARY KEY (`id`), ADD KEY `bonuses_leave_id_foreign` (`leave_id`), ADD KEY `bonuses_ibfk_1` (`salary_id`);

--
-- Indexes for table `employe_informations`
--
ALTER TABLE `employe_informations`
  ADD PRIMARY KEY (`id`), ADD KEY `employe_informations_company_id_foreign` (`company_id`);

--
-- Indexes for table `encashed_leaves`
--
ALTER TABLE `encashed_leaves`
  ADD PRIMARY KEY (`id`), ADD KEY `encashed_leaves_ibfk_1` (`employe_id`);

--
-- Indexes for table `foreign_purchases`
--
ALTER TABLE `foreign_purchases`
  ADD PRIMARY KEY (`id`), ADD KEY `foreign_purchases_company_id_foreign` (`company_id`);

--
-- Indexes for table `local_purchases`
--
ALTER TABLE `local_purchases`
  ADD PRIMARY KEY (`id`), ADD KEY `local_purchases_company_id_foreign` (`company_id`);

--
-- Indexes for table `local_sales`
--
ALTER TABLE `local_sales`
  ADD PRIMARY KEY (`id`), ADD KEY `local_sales_company_id_foreign` (`company_id`);

--
-- Indexes for table `nonregistered_purchases`
--
ALTER TABLE `nonregistered_purchases`
  ADD PRIMARY KEY (`id`), ADD KEY `nonregistered_purchases_company_id_foreign` (`company_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `oauth_access_tokens_id_session_id_unique` (`id`,`session_id`), ADD KEY `oauth_access_tokens_session_id_index` (`session_id`);

--
-- Indexes for table `oauth_access_token_scopes`
--
ALTER TABLE `oauth_access_token_scopes`
  ADD PRIMARY KEY (`id`), ADD KEY `oauth_access_token_scopes_access_token_id_index` (`access_token_id`), ADD KEY `oauth_access_token_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`), ADD KEY `oauth_auth_codes_session_id_index` (`session_id`);

--
-- Indexes for table `oauth_auth_code_scopes`
--
ALTER TABLE `oauth_auth_code_scopes`
  ADD PRIMARY KEY (`id`), ADD KEY `oauth_auth_code_scopes_auth_code_id_index` (`auth_code_id`), ADD KEY `oauth_auth_code_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `oauth_clients_id_secret_unique` (`id`,`secret`);

--
-- Indexes for table `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `oauth_client_endpoints_client_id_redirect_uri_unique` (`client_id`,`redirect_uri`);

--
-- Indexes for table `oauth_client_grants`
--
ALTER TABLE `oauth_client_grants`
  ADD PRIMARY KEY (`id`), ADD KEY `oauth_client_grants_client_id_index` (`client_id`), ADD KEY `oauth_client_grants_grant_id_index` (`grant_id`);

--
-- Indexes for table `oauth_client_scopes`
--
ALTER TABLE `oauth_client_scopes`
  ADD PRIMARY KEY (`id`), ADD KEY `oauth_client_scopes_client_id_index` (`client_id`), ADD KEY `oauth_client_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_grants`
--
ALTER TABLE `oauth_grants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_grant_scopes`
--
ALTER TABLE `oauth_grant_scopes`
  ADD PRIMARY KEY (`id`), ADD KEY `oauth_grant_scopes_grant_id_index` (`grant_id`), ADD KEY `oauth_grant_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`access_token_id`), ADD UNIQUE KEY `oauth_refresh_tokens_id_unique` (`id`);

--
-- Indexes for table `oauth_scopes`
--
ALTER TABLE `oauth_scopes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  ADD PRIMARY KEY (`id`), ADD KEY `oauth_sessions_client_id_owner_type_owner_id_index` (`client_id`,`owner_type`,`owner_id`);

--
-- Indexes for table `oauth_session_scopes`
--
ALTER TABLE `oauth_session_scopes`
  ADD PRIMARY KEY (`id`), ADD KEY `oauth_session_scopes_session_id_index` (`session_id`), ADD KEY `oauth_session_scopes_scope_id_index` (`scope_id`);

--
-- Indexes for table `office_expenses`
--
ALTER TABLE `office_expenses`
  ADD PRIMARY KEY (`id`), ADD KEY `office_expenses_company_id_foreign` (`company_id`);

--
-- Indexes for table `overtimes`
--
ALTER TABLE `overtimes`
  ADD PRIMARY KEY (`id`), ADD KEY `overtimes_employe_id_foreign` (`employe_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`), ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`), ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `salaries`
--
ALTER TABLE `salaries`
  ADD PRIMARY KEY (`id`), ADD KEY `salaries_employe_id_foreign` (`employe_id`);

--
-- Indexes for table `terminations`
--
ALTER TABLE `terminations`
  ADD PRIMARY KEY (`id`), ADD KEY `terminations_employe_id_foreign` (`employe_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`), ADD UNIQUE KEY `name` (`name`), ADD KEY `mail` (`mail`), ADD KEY `created` (`created`), ADD KEY `access` (`access`), ADD KEY `picture` (`picture`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oauth_access_token_scopes`
--
ALTER TABLE `oauth_access_token_scopes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_auth_code_scopes`
--
ALTER TABLE `oauth_auth_code_scopes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_client_grants`
--
ALTER TABLE `oauth_client_grants`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_client_scopes`
--
ALTER TABLE `oauth_client_scopes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_grant_scopes`
--
ALTER TABLE `oauth_grant_scopes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `oauth_session_scopes`
--
ALTER TABLE `oauth_session_scopes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique user ID.',AUTO_INCREMENT=214;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `allowances`
--
ALTER TABLE `allowances`
ADD CONSTRAINT `allowances_employe_id_foreign` FOREIGN KEY (`employe_id`) REFERENCES `employe_informations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bonuses`
--
ALTER TABLE `bonuses`
ADD CONSTRAINT `bonuses_ibfk_1` FOREIGN KEY (`salary_id`) REFERENCES `salaries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `bonuses_leave_id_foreign` FOREIGN KEY (`leave_id`) REFERENCES `encashed_leaves` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employe_informations`
--
ALTER TABLE `employe_informations`
ADD CONSTRAINT `employe_informations_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `encashed_leaves`
--
ALTER TABLE `encashed_leaves`
ADD CONSTRAINT `encashed_leaves_ibfk_1` FOREIGN KEY (`employe_id`) REFERENCES `employe_informations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `foreign_purchases`
--
ALTER TABLE `foreign_purchases`
ADD CONSTRAINT `foreign_purchases_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `local_purchases`
--
ALTER TABLE `local_purchases`
ADD CONSTRAINT `local_purchases_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `local_sales`
--
ALTER TABLE `local_sales`
ADD CONSTRAINT `local_sales_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nonregistered_purchases`
--
ALTER TABLE `nonregistered_purchases`
ADD CONSTRAINT `nonregistered_purchases_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
ADD CONSTRAINT `oauth_access_tokens_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_access_token_scopes`
--
ALTER TABLE `oauth_access_token_scopes`
ADD CONSTRAINT `oauth_access_token_scopes_access_token_id_foreign` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `oauth_access_token_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
ADD CONSTRAINT `oauth_auth_codes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_auth_code_scopes`
--
ALTER TABLE `oauth_auth_code_scopes`
ADD CONSTRAINT `oauth_auth_code_scopes_auth_code_id_foreign` FOREIGN KEY (`auth_code_id`) REFERENCES `oauth_auth_codes` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `oauth_auth_code_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
ADD CONSTRAINT `oauth_client_endpoints_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `oauth_client_grants`
--
ALTER TABLE `oauth_client_grants`
ADD CONSTRAINT `oauth_client_grants_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `oauth_client_grants_grant_id_foreign` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `oauth_client_scopes`
--
ALTER TABLE `oauth_client_scopes`
ADD CONSTRAINT `oauth_client_scopes_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `oauth_client_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_grant_scopes`
--
ALTER TABLE `oauth_grant_scopes`
ADD CONSTRAINT `oauth_grant_scopes_grant_id_foreign` FOREIGN KEY (`grant_id`) REFERENCES `oauth_grants` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `oauth_grant_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
ADD CONSTRAINT `oauth_refresh_tokens_access_token_id_foreign` FOREIGN KEY (`access_token_id`) REFERENCES `oauth_access_tokens` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
ADD CONSTRAINT `oauth_sessions_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `oauth_session_scopes`
--
ALTER TABLE `oauth_session_scopes`
ADD CONSTRAINT `oauth_session_scopes_scope_id_foreign` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `oauth_session_scopes_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `office_expenses`
--
ALTER TABLE `office_expenses`
ADD CONSTRAINT `office_expenses_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `overtimes`
--
ALTER TABLE `overtimes`
ADD CONSTRAINT `overtimes_employe_id_foreign` FOREIGN KEY (`employe_id`) REFERENCES `employe_informations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `salaries`
--
ALTER TABLE `salaries`
ADD CONSTRAINT `salaries_employe_id_foreign` FOREIGN KEY (`employe_id`) REFERENCES `employe_informations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `terminations`
--
ALTER TABLE `terminations`
ADD CONSTRAINT `terminations_employe_id_foreign` FOREIGN KEY (`employe_id`) REFERENCES `employe_informations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
