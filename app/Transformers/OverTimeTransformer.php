<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\OverTime;

/**
 * Class OverTimeTransformer
 * @package namespace App\Transformers;
 */
class OverTimeTransformer extends TransformerAbstract
{

    /**
     * Transform the \OverTime entity
     * @param \OverTime $model
     *
     * @return array
     */





    public function transform(OverTime $model)
    {
        return [
            'id'         => $model->id,
            'employe_id'=>$model->employe_id,
            'overtime_date'=>$model->overtime_date,
            'end_time'=>$model->end_time,
             'start_time'=>$model->start_time,
            'remark'=>$model->remark,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
