<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\LocalPurchase;

/**
 * Class LocalPurchaseTransformer
 * @package namespace App\Transformers;
 */
class LocalPurchaseTransformer extends TransformerAbstract
{

    /**
     * Transform the \LocalPurchase entity
     * @param \LocalPurchase $model
     *
     * @return array
     */
    public function transform(LocalPurchase $model)
    {
        $item_price=$model->item_price;
        $item_type=$model->item_type;
        $witholding=0;
         if($item_type==="imported"){
             $witholding=$item_price*0.03;

         }else{
             $witholding=$item_price*0.02;

         }

        return [
            'id' =>  $model->id,
            'company_name' => $model->company_name,
            'company_id' =>  $model->company_id,
            'item_price' =>  $model->item_price,
            'item_type'=>$model->item_type,
            'VAT'=>$item_price*0.15,
            'tin_number' =>  $model->tin_number,
            'withholding1_tax_number' =>  $model->withholding_tax_number,
            'withholding_tax'=>$witholding,
            'status'=>$model->status,
            'purchase_type' =>  $model->purchase_type,
            'mrc_code' =>  $model->mrc_code,
            'bill_code' =>  $model->bill_code,
            'item_name' =>  $model->item_name,
            'bill_date' =>  $model->bill_date,
            'withholding_date' =>  $model->withholding_date,
            'withholding_code' =>  $model->withholding_code,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
