<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EmployeInformation;

/**
 * Class EmployeInformationTransformer
 * @package namespace App\Transformers;
 */
class EmployeInformationTransformer extends TransformerAbstract
{

    /**
     * Transform the \EmployeInformation entity
     * @param \EmployeInformation $model
     *
     * @return array
     */
    public function transform(EmployeInformation $model)
    {
        return [
            'id'         => $model->id,
            'full_name' =>$model->full_name,
            'company_id'=>$model->company_id,
            'tin_number'=>$model->tin_number,
            'department'=>$model->department,
            'start_date'=>$model->start_date,
            'cost_sharing'=>$model->cost_sharing,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
