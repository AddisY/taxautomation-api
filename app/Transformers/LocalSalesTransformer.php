<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\LocalSales;

/**
 * Class LocalSalesTransformer
 * @package namespace App\Transformers;
 */
class LocalSalesTransformer extends TransformerAbstract
{

    /**
     * Transform the \LocalSales entity
     * @param \Lo-calSales $model
     *
     * @return array
     */
    public function transform(LocalSales $model)
    {
         $item_price =$model->item_price;

        return [
            'id' =>  $model->id,
            'company_id'=>$model->company_id,
            'item_name'=>$model->item_name,
            'item_price'=> (double) $model->item_price,
            'withholding_tax'=>$item_price*0.3,
            'status'=>$model->status,
            'bill_code'=>$model->bill_code,
            'bill_date'=>$model->bill_date,
            /* place your other model properties here */
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
