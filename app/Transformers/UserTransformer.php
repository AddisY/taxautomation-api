<?php
/**
 * Created by PhpStorm.
 * User: Mohammed
 * Date: 3/24/2016
 * Time: 6:06 PM
 */

namespace App\Transformers;

use App\Entities\Users\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
   // protected $availableIncludes = ['roles'];

    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'userType' => $user->user_type,
            'status' => $user->status
        ];
    }

    public function includeRoles(User $user)
    {
       // return $this->Collection($user->roles, new RoleTransformer());
    }
}