<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\EncashedLeave;

/**
 * Class EncashedLeaveTransformer
 * @package namespace App\Transformers;
 */
class EncashedLeaveTransformer extends TransformerAbstract
{

    /**
     * Transform the \EncashedLeave entity
     * @param \EncashedLeave $model
     *
     * @return array
     */
    public function transform(EncashedLeave $model)
    {
        return [
            'id'         =>  $model->id,
            'salary_id'=>$model->salary_id,
            'leave_date'=>$model->leave_date,
            'working_date'=>$model->working_date,
            'annual_leave'=>$model->annual_leave,
            'served_leave_period'=>$model->served_leaved_period,
            'leave_accumulated'=>$model->leave_accumulated,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
