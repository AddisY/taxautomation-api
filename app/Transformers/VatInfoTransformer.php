<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\VatInfo;

/**
 * Class VatInfoTransformer
 * @package namespace App\Transformers;
 */
class VatInfoTransformer extends TransformerAbstract
{

    /**
     * Transform the \VatInfoController entity
     * @param \VatInfoController $model
     *
     * @return array
     */
    public function transform(VatInfo $model)
    {
        return [
            'id'=> $model->id,
            'value_zero_goods'=>$model->value_zero_goods,
            'exempted_supply'=>$model->exempted_supply,
            'vat_returned_supply'=>$model->vat_returned_supply,
            'date'=>$model->date,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
