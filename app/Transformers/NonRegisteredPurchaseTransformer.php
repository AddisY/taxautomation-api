<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\NonRegisteredPurchase;

/**
 * Class NonRegisteredPurchaseTransformer
 * @package namespace App\Transformers;
 */
class NonRegisteredPurchaseTransformer extends TransformerAbstract
{

    /**
     * Transform the \NonRegisteredPurchaseController entity
     * @param \NonRegisteredPurchaseController $model
     *
     * @return array
     */
    public function transform(NonRegisteredPurchase $model)
    {
        return [
            'id'         => $model->id,
            'company_name'=>$model->company_name,
            'company_id'=>$model->company_id,
            'zone'=>$model->zone,
            'sub_city'=>$model->sub_city,
            'woreda'=>$model->woreda,
            'house_number'=>$model->house_number,
            'purchase_type'=>$model->purchase_type,
            'status'=>$model->status,
            'item_price'=>$model->item_price,
            'voucher_number'=>$model->voucher_number,
            'voucher_date'=>$model->voucher_date,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
