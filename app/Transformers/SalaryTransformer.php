<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Salary;

/**
 * Class SalaryTransformer
 * @package namespace App\Transformers;
 */
class SalaryTransformer extends TransformerAbstract
{

    /**
     * Transform the \Salary entity
     * @param \Salary $model
     *
     * @return array
     */
    public function transform(Salary $model)
    {
        return [
            'id'=>  $model->id,
            'total_salary'=>$model->total_salary,
            'employe_id'=>$model->employe_id,
            'payment_type'=>$model->payment_type,
            'duration'=>$model->duration,
            'pay_day'=>$model->pay_day,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
