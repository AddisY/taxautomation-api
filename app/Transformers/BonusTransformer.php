<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Bonus;

/**
 * Class BonusTransformer
 * @package namespace App\Transformers;
 */
class BonusTransformer extends TransformerAbstract
{

    /**
     * Transform the \Bonus entity
     * @param \Bonus $model
     *
     * @return array
     */
    public function transform(Bonus $model)
    {
        return [
            'id'=>  $model->id,
            'employe_id'=>$model->employe_id,
            'leave_id'=>$model->leave_id,
            'pay_date'=>$model->pay_day,
            'total_amount'=>$model->total_amount,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
