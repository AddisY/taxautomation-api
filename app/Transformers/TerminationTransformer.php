<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Termination;

/**
 * Class TerminationTransformer
 * @package namespace App\Transformers;
 */
class TerminationTransformer extends TransformerAbstract
{

    /**
     * Transform the \Termination entity
     * @param \Termination $model
     *
     * @return array
     */
    public function transform(Termination $model)
    {
        return [
            'id'         => $model->id,
            'employe-id'=>$model->employe_id,
            'end_date'=>$model->end_date,
            'type'=>$model->type,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
