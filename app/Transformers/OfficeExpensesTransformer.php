<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\OfficeExpenses;

/**
 * Class OfficeExpensesTransformer
 * @package namespace App\Transformers;
 */
class OfficeExpensesTransformer extends TransformerAbstract
{

    /**
     * Transform the \OfficeExpenses entity
     * @param \OfficeExpenses $model
     *
     * @return array
     */
    public function transform(OfficeExpenses $model)
    {
        return [
            'id'         =>  $model->id,
            'issue_date'=>$model->issue_date,
            'bill_code'=>$model->bill_code,
            'total_price'=>$model->total_price,
            'company_id'=>$model->company_id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
