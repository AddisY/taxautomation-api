<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ForeignPurchases;

/**
 * Class ForeignPurchasesTransformer
 * @package namespace App\Transformers;
 */
class ForeignPurchasesTransformer extends TransformerAbstract
{

    /**
     * Transform the \ForeignPurchases entity
     * @param \ForeignPurchases $model
     *
     * @return array
     */
    public function transform(ForeignPurchases $model)
    {
        $price=$model->tax_price;
        return [
            'id'         => $model->id,
            'dec_number'=>$model->dec_number,
            'company_id'=>$model->company_id,
            'dec_date'=>$model->dec_date,
            'bill_code'=>$model->bill_code,
            'tax_price'=>$model->tax_price,
            'status'=>$model->status,
            'item_price'=>$model->item_price,

            'VAT'=>$price*0.15,
            'item_name'=>$model->item_name,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
