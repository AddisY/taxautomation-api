<?php

namespace App\Transformers;

use App\Entities\LocalSales;
use App\Entities\Users\User;
use League\Fractal\TransformerAbstract;


/**
 * Class VATReportTransformer
 * @package namespace App\Transformers;
 */
class VATReportTransformer extends TransformerAbstract
{


    public function transform(User $model)
    {
        return [
            'localgoodspurchase_vat' => $model->localgoodspurchase_vat,

            'localgoodspurchase' => $model->localgoodspurchase,
            'localgoodspurchase_status'=>$model->localgoodspurchase_status,
            'localservicespurchase_vat' => $model->localservicespurchase_vat,
            'localservicespurchase' => $model->localservicepurchase,
            'localservicespurchase_status' => $model->localservicepurchase_satatus,
            'foreignPurchaseSummery' => $model->foreignPurchaseSummery,
            'foreignPurchaseSummery_status' => $model->foreignPurchaseSummery_status,
            'foreignPurchaseSummery_vat' => $model->foreignPurchaseSummery_vat,
            'salessummery' => $model->salessummery,
            'salessummery_status' => $model->salessummery_status,
            'salessummery_vat' => $model->salessummery_vat,
            'nontinPurchase_status' => $model->nontinPurchase_status,
            'nontinPurchase' => $model->nontinPurchase,
            'total_input_value' => $model->total_input_value,
            'net_vat_due' => $model->net_vat_due,
            'vat_credit' => $model->vat_credit,
            'vat_status' => round($model->total_status)


        ];
    }
}
