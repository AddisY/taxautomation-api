<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Allowance;

/**
 * Class AllowanceTransformer
 * @package namespace App\Transformers;
 */
class AllowanceTransformer extends TransformerAbstract
{

    /**
     * Transform the \Allowance entity
     * @param \Allowance $model
     *
     * @return array
     */
    public function transform(Allowance $model)
    {
        return [
            'id'         =>  $model->id,
            'employe_id'=>$model->employe_id,
            'type'=>$model->type,
            'total_amount'=>$model->total_amount,
            'start_date'=>$model->start,
            'end_date'=>$model->end,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
