<?php

namespace App\Transformers;

use App\Entities\Users\User;
use League\Fractal\TransformerAbstract;

class UserTransformerDropDown extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name
        ];
    }
}