<?php

namespace App\Entities;

use App\Entities\Users\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class OfficeExpenses extends Model implements Transformable
{
    use TransformableTrait;
    protected $table='office_expenses';

    protected $fillable = ['issue_date','bill_code','total_price'];

    public function company(){

        return $this->belongsTo(User::class,'company_id');
    }

}
