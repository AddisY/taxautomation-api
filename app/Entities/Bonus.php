<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Bonus extends Model implements Transformable
{
    use TransformableTrait;
    protected $table='bonuses';

    protected $fillable = ['employe_id','pay_day','total_amount',
        'served_leaved_period','status'];
    public  function  employe(){

        return $this->belongsTo(EmployeInformation::class,'employe_id');
    }


}
