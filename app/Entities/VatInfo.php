<?php

namespace App\Entities;

use App\Entities\Users\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class VatInfo extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['company_id','value_zero_goods','exempted_supply','vat_returned_supply'];
    public function company(){
        $this->belongsTo(User::class,'company_id');
    }

}
