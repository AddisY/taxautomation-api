<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Allowance extends Model implements Transformable
{
    use TransformableTrait;
    protected $table='allowances';

    protected $fillable = ['employe_id','type','description','total_amount','end','start','status'];

    public function employeInfo(){
        return $this->belongsTo(EmployeInformation::class,'employe_id');
    }

}
