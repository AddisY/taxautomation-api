<?php

namespace App\Entities;

use App\Entities\Users\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ForeignPurchases extends Model implements Transformable
{
    use TransformableTrait;
    protected $table='foreign_purchases';


    protected $fillable = ['dec_number','company_id',
    'dec_date','bill_code','tax_price','item_price','item_name'];

   public function companyProfile(){
       return $this->belongsTo(User::class,'company_id');
   }

}
