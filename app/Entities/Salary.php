<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Salary extends Model implements Transformable
{
    use TransformableTrait;
    protected $table='salaries';

    protected $fillable = ['total_salary','pay_day','employe_id','payment_type','duration','status'];

    public function employeInfo(){
        return $this->belongsTo(EmployeInformation::class,'employe_id');
    }



}
