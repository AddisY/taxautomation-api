<?php

namespace App\Entities;

use App\Entities\Users\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class EmployeInformation extends Model implements Transformable
{
    use TransformableTrait;

    protected $table='employe_informations';

    protected $fillable = ['full_name','company_id','tin_number','department','start_date',
    'cost_sharing','status'];

    public function company(){

    return $this->belongsTo(User::class,'company_id');
   }
    public function salary(){
        return $this->hasMany(Salary::class,'employe_id');
    }
    public function overtime(){
        return $this->hasMany(OverTime::class,'employe_id');
    }
    public function termination(){
        return $this->hasOne(Termination::class,'employe_id');}

    public function allowance(){
        return $this->hasMany(Allowance::class,'employe_id');
    }
    public function bonus()
    {
        return $this->hasMany(Bonus::class,'employe_id');
    }
    public function leave(){
        return $this->hasMany(EncashedLeave::class,'employe_id');
    }

}
