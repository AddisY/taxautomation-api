<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class EncashedLeave extends Model implements Transformable
{
    use TransformableTrait;
    protected $table='encashed_leaves';

    protected $fillable = ['employe_id','leave_date','working_date',
        'annual_leave','served_leaved_period','leave_accumlated','issued_date'];

    public function employeInfo(){
        return $this->belongsTo(EmployeInformation::class,'employe_id');
    }


}
