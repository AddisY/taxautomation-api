<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Termination extends Model implements Transformable
{
    use TransformableTrait;
    protected $table='terminations';

    protected $fillable = ['employe_id','end_date','type','status'];
    public function employeInfo(){
        return $this->belongsTo(EmployeInformation::class,'employe_id');
    }

}
