<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class NonRegisteredPurchase extends Model implements Transformable
{
    use TransformableTrait;
    protected $table='nonregistered_purchases';

    protected $fillable = ['company_name','company_id','zone','sub_city','woreda',
    'house_number','purchase_type','item_price','voucher_date'];

    public function companyProfile(){
        return $this->belongsTo(User::class,'company_id');
    }

}
