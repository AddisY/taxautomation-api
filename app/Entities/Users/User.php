<?php

namespace App\Entities\Users;

use App\Entities\EmployeInformation;
use App\Entities\ForeignPurchases;
use App\Entities\LocalPurchase;
use App\Entities\LocalSales;
use App\Entities\NonRegisteredPurchase;
use App\Entities\OfficeExpenses;
use App\Entities\VatInfo;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Class User
 *
 * @package App\Entities\Users
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;




//    const STATUS_ACTIVE = 'active';
//    const STATUS_INACTIVE = 'inactive';
//    const USER_TYPE_ADMIN = 'admin';

//    use Authenticatable, CanResetPassword, SoftDeletes, EntrustUserTrait {
//        EntrustUserTrait::restore insteadof SoftDeletes;
//    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'mail', 'pass', 'first_name', 'mobile_number', 'tin_number', 'last_name', 'status'];


    public function foreignPurchase()
    {

        return $this->hasMany(ForeignPurchases::class, 'company_id');
    }

    public function localPurchase()
    {

        return $this->hasMany(LocalPurchase::class, 'company_id');
    }

    public function localSales()
    {

        return $this->hasMany(LocalSales::class, 'company_id');
    }

    public function nonregisteredPurchase()
    {

        return $this->hasMany(NonRegisteredPurchase::class, 'company_id');
    }

    public function employe()
    {
        return $this->hasMany(EmployeInformation::class, 'company_id');
    }

    public function officeexp()
    {
        return $this->hasOne(OfficeExpenses::class, 'company_id');
    }
    public function vatinfo(){
        return $this->hasMany(VatInfo::class,'company_id');
    }
}
