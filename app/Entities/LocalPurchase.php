<?php

namespace App\Entities;

use App\Entities\Users\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class LocalPurchase extends Model implements Transformable
{
    use TransformableTrait;
    protected  $table = 'local_purchases';

    protected $fillable = ['company_name','item_price','tin_number','withholding_tax_number','purchase_type',
    'mrc_code','bill_code','item_name','bill_date','withholding_date','withholding_code,'];

    public function company(){

    return $this->belongsTo(User::class,'company_id');
}
    public function companyProfile(){
        return $this->belongsTo(User::class,'company_id');
    }

}
