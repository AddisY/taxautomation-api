<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class OverTime extends Model implements Transformable
{
    use TransformableTrait;
    protected $table='overtimes';

    protected $fillable = ['employe_id','overtime_date','start_time','end_time','status'];

    public function employeInfo(){

        return $this->belongsTo(EmployeInformation::class,'employe_id');
    }

}
