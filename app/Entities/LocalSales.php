<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class LocalSales extends Model implements Transformable
{
    use TransformableTrait;
    protected $table='local_sales';

    protected $fillable = ['company_id','item_name','item_price','bill_code','bill_date'];
    public function companyProfile(){
        return $this->belongsTo(User::class,'company_id');
    }

}
