<?php
/**
 * Created by PhpStorm.
 * User: Mohammed
 * Date: 5/18/2016
 * Time: 3:31 PM
 */

namespace App\Entities\Traits;

use Illuminate\Http\Request;
use Sofa\Eloquence\Eloquence;

trait UserTrait
{
    use Eloquence;

    public function scopeUserSearch($query, Request $request)
    {
        if ($request->has('search')) {
            $search_key = $request->get('search');
            $query = $query->search($search_key, ['name', 'email']);
        }

        return $query;
    }

    public function scopeFilterByStatus($query, Request $request)
    {
        if ($request->has('status')) {
            $type = $request->get('status');
            $types = explode(",", $type);
            $query->whereIn('status',$types);
        }

        return $query;
    }
}