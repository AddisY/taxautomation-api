<?php

namespace App\Presenters;

use App\Transformers\OfficeExpensesTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OfficeExpensesPresenter
 *
 * @package namespace App\Presenters;
 */
class OfficeExpensesPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OfficeExpensesTransformer();
    }
}
