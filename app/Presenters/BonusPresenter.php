<?php

namespace App\Presenters;

use App\Transformers\BonusTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BonusPresenter
 *
 * @package namespace App\Presenters;
 */
class BonusPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BonusTransformer();
    }
}
