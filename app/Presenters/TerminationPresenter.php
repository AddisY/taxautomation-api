<?php

namespace App\Presenters;

use App\Transformers\TerminationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TerminationPresenter
 *
 * @package namespace App\Presenters;
 */
class TerminationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TerminationTransformer();
    }
}
