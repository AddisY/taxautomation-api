<?php

namespace App\Presenters;

use App\Transformers\VatInfoTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class VatInfoPresenter
 *
 * @package namespace App\Presenters;
 */
class VatInfoPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new VatInfoTransformer();
    }
}
