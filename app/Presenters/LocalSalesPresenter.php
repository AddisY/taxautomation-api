<?php

namespace App\Presenters;

use App\Transformers\LocalSalesTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class LocalSalesPresenter
 *
 * @package namespace App\Presenters;
 */
class LocalSalesPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new LocalSalesTransformer();
    }
}
