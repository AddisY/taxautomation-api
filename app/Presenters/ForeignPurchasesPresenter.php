<?php

namespace App\Presenters;

use App\Transformers\ForeignPurchasesTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ForeignPurchasesPresenter
 *
 * @package namespace App\Presenters;
 */
class ForeignPurchasesPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ForeignPurchasesTransformer();
    }
}
