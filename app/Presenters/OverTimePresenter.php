<?php

namespace App\Presenters;

use App\Transformers\OverTimeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OverTimePresenter
 *
 * @package namespace App\Presenters;
 */
class OverTimePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OverTimeTransformer();
    }
}
