<?php

namespace App\Presenters;

use App\Transformers\AllowanceTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AllowancePresenter
 *
 * @package namespace App\Presenters;
 */
class AllowancePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AllowanceTransformer();
    }
}
