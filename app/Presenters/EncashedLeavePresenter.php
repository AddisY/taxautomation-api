<?php

namespace App\Presenters;

use App\Transformers\EncashedLeaveTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EncashedLeavePresenter
 *
 * @package namespace App\Presenters;
 */
class EncashedLeavePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EncashedLeaveTransformer();
    }
}
