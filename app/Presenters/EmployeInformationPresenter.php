<?php

namespace App\Presenters;

use App\Transformers\EmployeInformationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmployeInformationPresenter
 *
 * @package namespace App\Presenters;
 */
class EmployeInformationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmployeInformationTransformer();
    }
}
