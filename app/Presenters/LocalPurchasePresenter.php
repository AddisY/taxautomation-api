<?php

namespace App\Presenters;

use App\Transformers\LocalPurchaseTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class LocalPurchasePresenter
 *
 * @package namespace App\Presenters;
 */
class LocalPurchasePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new LocalPurchaseTransformer();
    }
}
