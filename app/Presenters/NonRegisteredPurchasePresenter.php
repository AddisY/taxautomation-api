<?php

namespace App\Presenters;

use App\Transformers\NonRegisteredPurchaseTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class NonRegisteredPurchasePresenter
 *
 * @package namespace App\Presenters;
 */
class NonRegisteredPurchasePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new NonRegisteredPurchaseTransformer();
    }
}
