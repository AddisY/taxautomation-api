<?php

namespace App\Repositories;


use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\NonRegisteredPurchase;
use App\Validators\NonRegisteredPurchaseValidator;
use Webpatser\Uuid\Uuid;



/**
 * Class NonRegisteredPurchaseRepository
 * @package namespace App\Repositories;
 */
class NonRegisteredPurchaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NonRegisteredPurchase::class;
    }

    public function getallnonregisteredPurchases($per_page){
        $nonregPurchase=NonRegisteredPurchase::paginate($per_page);
        return $nonregPurchase;
    }
    public  function getbyCompany($company_id){
        $nonregPurchasesd=NonRegisteredPurchase::where('company_id',$company_id)->get();
        return $nonregPurchasesd;
    }
    public  function getnonregisteredPurchases($id){
        $nonregPurchase=NonRegisteredPurchase::find($id);
        return $nonregPurchase;

    }
    public function getnonregisteredBydate($date,$uid){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $nonregPurchase = NonRegisteredPurchase::where('company_id',$uid)
            ->whereBetween('voucher_date',[$from,$to]);
        return $nonregPurchase;
    }
    public function storenonregisteredPurchase($inputs){
        $nonregPurchases[]=null;
        $i=0;
        foreach($inputs->nonregpurchase as $input){
            $nonregPurchase=new NonRegisteredPurchase();
            $nonregPurchase->id=Uuid::generate();
            $nonregPurchase->company_name= $input['company_name'];
            $nonregPurchase->company_id= $input['company_id'];
            $nonregPurchase->status =0;
            $nonregPurchase->zone= $input['zone'];
            $nonregPurchase->sub_city= $input['sub_city'];
            $nonregPurchase->woreda= $input['woreda'];
            $nonregPurchase->house_number= $input['house_number'];
            $nonregPurchase->purchase_type= $input['purchase_type'];
            $nonregPurchase->item_price= $input['item_price'];
            $nonregPurchase->voucher_number= $input['voucher_number'];
            $nonregPurchase->voucher_date= $input['voucher_date'];
            $nonregPurchase->save();
            $nonregPurchases[$i]=$nonregPurchase;
            $i++;
        }

        return $nonregPurchases;

    }
    public function updatenonregisteredPurchase($inputs){
        $nonregPurchases[]=null;
        $i=0;
        foreach($inputs->nonregpurchase as $input){
            $nonregPurchase=NonRegisteredPurchase::find($input['id']);

            $nonregPurchase->company_name= $input['company_name'];
            $nonregPurchase->company_id= $input['company_id'];
            $nonregPurchase->status =0;
            $nonregPurchase->zone= $input['zone'];
            $nonregPurchase->sub_city= $input['sub_city'];
            $nonregPurchase->woreda= $input['woreda'];
            $nonregPurchase->house_number= $input['house_number'];
            $nonregPurchase->purchase_type= $input['purchase_type'];
            $nonregPurchase->item_price= $input['item_price'];
            $nonregPurchase->voucher_number= $input['voucher_number'];
            $nonregPurchase->voucher_date= $input['voucher_date'];
            $nonregPurchase->save();
            $nonregPurchases[$i]=$nonregPurchase;
            $i++;
        }

        return $nonregPurchases;

    }
    public function deletenonregisteredPurchase($ids){
        $nonregarray=array();
        $i=0;
        foreach($ids->nonregPurchase as $id){
            $nonregPurchase= NonRegisteredPurchase::find($id['id']);
            $nonregPurchase->delete();
            $nonregPurchase[$i]=[
                'nonregPurchase'=>$nonregPurchase
            ];
            $i++;
        }

        return $nonregarray;
    }
}
