<?php

namespace App\Repositories;

use App\Entities\EmployeInformation;
use App\Entities\Users\User;
use Carbon\Carbon;
use DateTime;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\OverTime;
use App\Validators\OverTimeValidator;
use Webpatser\Uuid\Uuid;



/**
 * Class OverTimeRepository
 * @package namespace App\Repositories;
 */
class OverTimeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OverTime::class;
    }
    public function getAllovertime($company_id){
        $overTime=EmployeInformation::where('company_id',$company_id)->get();
        $overArray=array();
        $i=0;
        foreach($overTime as $over){
            $overArray[$i]=[
                'overtime'=>$over->overtime
            ];
            $i++;
        }
        return $overArray;
    }
    public function getOvertimeByemployeID($employe_id){
        $overTime=OverTime::where('employe_id',$employe_id)
            ->get();
        return $overTime;
    }
    public function getOvertime($id){
        $overTime=OverTime::find($id);
        return $overTime;
    }

    function isWeekend($date) {
        return (date('N', strtotime($date)) >= 6);
    }

    private  function isPublicHoliday($date){
        $result = false;
        $month=intval(date("m", strtotime($date)));
        $day=intval(date("m", strtotime($date)));
        if($month===1 &&($day===7||$day=19)){
            $result= true;
        }elseif($month===3 &&($day===2||$day=20)){
            $result=true;

        }elseif($month===4 &&($day===16||$day=14)){
            $result=true;
        }elseif($month===5 &&($day===1||$day=5 || $day===28)){
            $result =true;

        }elseif($month===6 &&($day===21||$day=26)){
            $result=true;

        }elseif($month===9 &&($day===2||$day===11||$day===22||$day===27)){
            $result = true;
        }elseif($month===11 &&($day===1||$day=21)){
            $result=true;
        }else{
            $result=false;
        }

        return $result;

    }

    private function overtimeCalculation($start_time,$end_time,$ot_date){
        $start = explode(':', $start_time);
        $end = explode(':', $end_time);

        if($end[0]===$start[0]){
            $total_hours= ($end[0]-$start[0]).':'.($end[1]-$start[1]);
        }
       elseif($end[1]===$start[1]){
            $total_hours=$end[0]-$start[0].':'.$end[1]-$start[1];
        }else{
            $total_hours =($end[0]-$start[0]).':'.($end[1]-$start[1]);
        }
        $hours =explode(':',$total_hours);

        if(intval($start[0])<22&& intval($start[0])>5 ){
            $performance= (intval($hours[0])*1.25 )+((intval($hours[1])/60)*1.25);

        }elseif(intval($start[0])>22 && intval($start[0])<=5){
            $performance= (intval($hours[0])*1.50 )+((intval($hours[1])/60)*1.50);
        }elseif($this->isWeekend($ot_date)){
            $performance= (intval($hours[0])*2.00 )+((intval($hours[1])/60)*2.00);
        }elseif($this->isPublicHoliday($ot_date)){
            $performance= (intval($hours[0])*2.50 )+((intval($hours[1])/60)*2.50);
        }else{
            $performance=0;
        }

        return $performance;
    }

    public function getovertimeBycompanydate($company_id,$date){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $overTimes = EmployeInformation::
        with(['salary','overtime'=>function($query) use($from,$to){
            $query->whereBetween('overtime_date',[$from,$to]);
        }])
            ->has('salary')
            ->has('overtime')
            ->where('company_id',$company_id)->get();


        $employeinfo[]=null;
        $salary[]=null;
        $hourlyrate=0;
        $aggrigation[]=null;
        $j=0;
        $i=0;

        $a=0;
  foreach($overTimes as $overTime){

      $overtimearray=array();
      $h=0;

           $employeinfo=[
               'Employe ID'=>$overTime->id,
               'Name'=>$overTime->full_name,
               'Department'=>$overTime->department
           ];

                $hourlyrate=$overTime->salary->last()->total_salary/208;
                foreach($overTime->overtime as $overtimes){
                    $overtimearray[$h]=[
                        'employeid'=>$overtimes->employe_id,
                        'Basic Salary'=>$overTime->salary->last()->total_salary,
                        'Hourly_Rate'=>$hourlyrate,
                        'before_four'=>$this->overtimeCalculation($overtimes->start_time,$overtimes->end_time,$overtimes->overtime_date),
                        'after_four'=>$this->overtimeCalculation($overtimes->start_time,$overtimes->end_time,$overtimes->overtime_date),
                        'onweekend'=>$this->overtimeCalculation($overtimes->start_time,$overtimes->end_time,$overtimes->overtime_date),
                        'onpublicHoliday'=>$this->overtimeCalculation($overtimes->start_time,$overtimes->end_time,$overtimes->overtime_date)
                    ];
                    $h++;
                }

          $aggrigation[$a]=[
              'employe'=>$employeinfo,
              'overtime'=>$overtimearray
          ];
         $a++;
        }
       return $aggrigation;

    }

    public function storeOverTime($inputs){
        $i=0;
        $overtimes[]=null;
        foreach($inputs->overtime as $input){

            $extraTime= new OverTime();
            $extraTime->id=Uuid::generate();
            $extraTime->employe_id=$input['employe_id'];
            $extraTime->overtime_date=$input['overtime_date'];
           // $extraTime->status=0;
            $extraTime->start_time=$input['start_time'];
            $extraTime->end_time=$input['end_time'];
            $extraTime->remark=$input['remark'];
            $extraTime->save();
            $overtimes[$i]=$extraTime;
            $i++;

        }
        return $overtimes;

    }
    public  function updateOvertime($inputs){
        $i=0;
        $overtimes[]=null;
        foreach($inputs->overtime as $input){

            $extraTime= OverTime::find($input['id']);
            $extraTime->overtime_date=$input['overtime_date'];
            $extraTime->start_time=$input['start_time'];
            $extraTime->end_time=$input['end_time'];
            $extraTime->remark=$input['remark'];
            $extraTime->save();
            $overtimes[$i]=$extraTime;
            $i++;

        }
        return $overtimes;
    }
    public  function deleteOverTime($id){
        $overtime=OverTime::find($id);
        $overtime->delete();
        return $overtime;
    }



}
