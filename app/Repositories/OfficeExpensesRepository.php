<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\OfficeExpenses;
use App\Validators\OfficeExpensesValidator;
use Webpatser\Uuid\Uuid;

;

/**
 * Class OfficeExpensesRepository
 * @package namespace App\Repositories;
 */
class OfficeExpensesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OfficeExpenses::class;
    }

  public function getAllofficeexp($company_id){

      $officeexp=OfficeExpenses::where('company_id',$company_id)->get();

      return $officeexp;
  }
    public  function getofficeexp($id,$company_id){
        $officeexp=OfficeExpenses::where('id',$id)
            ->where('company_id',$company_id)
            ->get();
        return $officeexp;
    }

    public function storeOfficeexp($input){
        $officeExp=new OfficeExpenses();
        $officeExp->id=Uuid::generate();
        $officeExp->issue_date=$input->officeexp['issue_date'];
        $officeExp->bill_code= $input->officeexp['bill_code'];
        $officeExp->status=0;
        $officeExp->total_price=$input->officeexp['total_price'];
        $officeExp->company_id=$input->officeexp['company_id'];
        $officeExp->save();
        return $officeExp;
    }



    public  function updateOfficeExp($input,$id){

        $officeExp= OfficeExpenses::find($id);
        $officeExp->issue_date=$input->officeexp['issue_date'];
        $officeExp->bill_code= $input->officeexp['bill_code'];
        $officeExp->total_price=$input->officeexp['total_price'];
        $officeExp->save();
        return $officeExp;


    }

    public function deleteOfficeexp($id){
        $officeExp=OfficeExpenses::find($id);
        $officeExp->delete();
        return $officeExp;

    }
}
