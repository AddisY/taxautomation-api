<?php

namespace App\Repositories;

use App\Entities\VatInfo;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VatInfoRepository
 * @package namespace App\Repositories;
 */
Class VatInfoRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return VatInfo::class;
    }

    //['company_id','value_zero_goods','exempted_supply','vat_returned_supply'];



    public function getVatinfoBydate($company_id,$date){
        $vatInfo=VatInfo::where('company_id',$company_id)->where('created_at',$date)->first();
        return $vatInfo;

    }
    public function storevatInfo($input){
        $vatInfo=new VatInfo();
        $vatInfo->company_id= $input->vatinfo['company_id'];
        $vatInfo->value_zero_goods= $input->vatinfo['value_zero_goods'];
        $vatInfo->exempted_supply=$input->vatinfo['exempted_supply'];
        $vatInfo->vat_returned_supply=$input->vatinfo['vat_returned_supply'];
        $vatInfo->date=$input->vatinfo['date'];
        $vatInfo->save();
        return $vatInfo;
    }
    public function updatevatInfo($input,$id){
        $vatInfo=VatInfo::find($id);
        $vatInfo->value_zero_goods = $input->vatinfo['value_zero_goods'];
        $vatInfo->exempted_supply = $input->vatinfo['exempted_supply'];
        $vatInfo->vat_returned_supply = $input->vatinfo['vat_returned_supply'];
        $vatInfo->date = $input->vatinfo['date'];
        $vatInfo->save();
        return $vatInfo;
    }
    public function deletevatInfo($id){

        $vatInfo=VatInfo::find($id);
        $vatInfo->delete();
        return $vatInfo;
    }







}
