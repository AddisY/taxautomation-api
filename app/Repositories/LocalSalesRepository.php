<?php

namespace App\Repositories;

use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\LocalSales;
use App\Validators\LocalSalesValidator;
use Webpatser\Uuid\Uuid;

;

/**
 * Class LocalSalesRepository
 * @package namespace App\Repositories;
 */
class LocalSalesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LocalSales::class;
    }

    public function  getalllocalSales($per_page){
        $localSales=LocalSales::paginate($per_page);
        return $localSales;
    }


    public function  getbycompany($company_id){

        $localsales=LocalSales::where('company_id',$company_id)->get();
        return $localsales;

    }
    public  function getlocalSales($id){
        $localSales=LocalSales::finde($id);
        return $localSales;
    }
    public function getlocalsalesBydate($date,$uid){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $localSales=LocalSales::where('company_id',$uid)
            ->whereBetween('bill_date',[$from,$to]);
        return $localSales;
    }

    public function getlocalSalesSummery($date){
       // return $date;
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $salessummery=LocalSales::whereBetween('bill_date',[$from,$to])->sum('item_price');
        $localSales = new LocalSales();
        $localSales->salessummery =$salessummery;
         $localSales->summery_with_vat=$salessummery*0.15;
        return $localSales;

    }

   public function  storelocalSales($inputs)
   {
       $localSaleses[]=null;
       $i=0;
       foreach($inputs->localsales as $input){
           $localSales = new LocalSales();
           $localSales->id = Uuid::generate();
           $localSales->company_id = $input['company_id'];
           $localSales->item_name = $input['item_name'];
           $localSales->item_price = $input['item_price'];
           $localSales->bill_code = $input['bill_code'];
           $localSales->bill_date = $input['bill_date'];
           $localSales->status =0;
           $localSales->save();
           $localSaleses[$i]=$localSales;
           $i++;
       }
       return $localSaleses;
   }

       public function  updatelocalSales($inputs){
           $localSaleses[]=null;
           $i=0;
           foreach($inputs->localsales as $input){
               $localSales = LocalSales::find($input["id"]);
               $localSales->item_name = $input['item_name'];
               $localSales->item_price = $input['item_price'];
               $localSales->bill_code = $input['bill_code'];
               $localSales->bill_date = $input['bill_date'];
               $localSales->status =0;
               $localSales->save();
               $localSaleses[$i]=$localSales;
               $i++;
           }
           return $localSaleses;
       }
    public  function deletelocalSales($ids){
        $localsalesarray=array();
        $i=0;

        foreach($ids->localsales as $id){

            $localSales=LocalSales::find($id['id']);
            $localSales->delete();
            $localsalesarray[$i]=[
                'localsales'=>$localSales
            ];
            $i++;
        }

        return $localsalesarray;
    }
}
