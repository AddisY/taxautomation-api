<?php

namespace App\Repositories;

use App\Entities\EmployeInformation;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\EncashedLeave;
use App\Validators\EncashedLeaveValidator;
use Webpatser\Uuid\Uuid;



/**
 * Class EncashedLeaveRepository
 * @package namespace App\Repositories;
 */
class EncashedLeaveRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EncashedLeave::class;
    }
    public function getAllLeave($company_id){
      $leaves=EmployeInformation::where('company_id',$company_id)->get();
      $encashedArry=array();
      $i=0;
      foreach($leaves as $leave){
          if(count($leave->leave)){
              $encashedArry[$i]=[
                  'leave'=>$leave->leave
              ];
              $i++;
          }
      }
      return $encashedArry;
  }
    public function getLeave($id,$employe_id){
        $leave=EncashedLeave::where('id',$id)
            ->where('employe_id',$employe_id)
            ->get();
        return $leave;
    }
    private  function getEncashedleaveincomeTax($workingDay,$accumlatedLeave,$salary,$served_day,$served_month,$rounddedAcculatedleave){
        $grossEncashedleave= $this->getgrossEarnedencashedLeave($accumlatedLeave,$workingDay,$served_day,$salary,$rounddedAcculatedleave);
        $value=($salary+($grossEncashedleave/$served_month));
        $result=0;
        $result2=0;




     return ($result*$served_month)-($result2*$served_month);

    }
    private  function getincometaxVariableencasheleave($monthlyTaxable,$grossencashed){
        $value =$monthlyTaxable;
        $result=0;
        if($value<=600 && $grossencashed>0) {
            $result = $monthlyTaxable*0;
        }
        elseif($value<=1650 && $grossencashed>0){
            $result=$monthlyTaxable*0.1-60;
        }elseif($value<=3200  && $grossencashed>0) {
            $result = $monthlyTaxable * 0.15 - 142.50;
        }elseif($value<=5250 && $grossencashed>0){
            $result=$monthlyTaxable * 0.2 - 302.50;
        }elseif($value<=7800  && $grossencashed>0){
            $result=$monthlyTaxable *0.25 - 565;
        }elseif($value<=10900  && $grossencashed>0){
            $result=$monthlyTaxable * 0.3 - 955;
        }elseif($value>10900  && $grossencashed>0){
            $result=$monthlyTaxable* 0.35 - 1500;
        }elseif($grossencashed<=0){
            $result=0;
        }
        return $result;
    }
    private function getgrossEarnedencashedLeave($accumlatedLeave,$workingDay,$served_day,$salary,$roundedaccumlatedleave){
        $result=0;
        if($accumlatedLeave>=$workingDay){
            $result=($salary+((($roundedaccumlatedleave-$workingDay)/$workingDay)*$salary))+($served_day/($workingDay*12)*$salary);

        }elseif($accumlatedLeave<$workingDay){
            $result=(($roundedaccumlatedleave/$workingDay) *$salary) +($served_day/($workingDay*12)*$salary);
        }
        return $result;

    }
    private function getgencashedEarnedleave($workingday,$served_days,$salary,$accumlated_leav,$roundedaccumlatedLeave){
        $result=0;
        if($accumlated_leav>=$workingday){
            $result=($salary+((($roundedaccumlatedLeave-$workingday)/$workingday)*$salary))+($served_days/($workingday*12)*$salary);

        }elseif($accumlated_leav<$workingday){
            $result=(($roundedaccumlatedLeave/$workingday)*$salary)+($served_days/($workingday*12)*$salary);
        }
        return $result;
    }
    private function getnetAccumlatedleav($served_period,$working_date,$accumulated_leave){


        $served_periods= explode("-",$served_period);
        $result = (($served_periods[0]/12)*$accumulated_leave)+($served_periods[1]/($working_date*12));
        return $result;
    }
    private function getgrossEarnedencashedLeavebymonth($grossearned,$servedmonth,$numberofmonth){
         $result=0;

       if(($grossearned/$servedmonth)*$numberofmonth <= $grossearned){
           $result=$grossearned/$servedmonth;
       }
          return $result;
      }
    public function getvariableEncashedBycompany($company_id,$date){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $emploies=EmployeInformation::with(['salary'
             => function($query){
            $query->where('payment_type','variable');}
            ,'leave'=>function ($que) use($from,$to){
                $que->whereBetween('issued_date',[$from,$to]);
            }])
            ->has('leave')
            ->where('company_id',$company_id)->get();
        $employeArry=array();
        $e=0;
        $aggregateArray=array();
        $a=0;
        foreach($emploies as $employe){
            $salaryArry=array();
            $s=0;
            if(!count($employe->leave)==0) {
                $net_accumelated_leave=$this->getnetAccumlatedleav($employe->leave[0]->served_leaved_period,$employe->leave[0]->working_date,$employe->leave[0]->leave_accumulated);
                  $served_period = explode('-', $employe->leave[0]->served_leaved_period);
                 $net_accumelated_leave_rounded=round($this->getnetAccumlatedleav($employe->leave[0]->served_leaved_period,$employe->leave[0]->working_date,$employe->leave[0]->leave_accumulated));
              $employeArry = [
                    'employeid' => $employe->id,
                    'full_name' => $employe->full_name,
                    'annual_leave' => $employe->leave[0]->annual_leave,
                    'working_day' => $employe->leave[0]->working_date,
                    'served_period' => $employe->leave[0]->served_leaved_period,
                    'accumulated_leave' => $employe->leave[0]->leave_accumulated,
                    'net_accumelated_leave'=>$this->getnetAccumlatedleav($employe->leave[0]->served_leaved_period,$employe->leave[0]->working_date,$employe->leave[0]->leave_accumulated),
                    'net_accumelated_leave_rounded'=>round($this->getnetAccumlatedleav($employe->leave[0]->served_leaved_period,$employe->leave[0]->working_date,$employe->leave[0]->leave_accumulated)),
                    'final_taxable_salary' =>$employe->salary->last()['total_salary'],
                    'gross_encashed_leave' => $this->getgrossEarnedencashedLeave($net_accumelated_leave,
                      $employe->leave[0]->working_date, $served_period[1],$employe->salary->last()['total_salary'],$net_accumelated_leave_rounded)
                ];
                foreach ($employe->salary as $salaries) {
                    $served_period = explode('-', $employe->leave[0]->served_leaved_period);
                    $net_accumulated_leave_day = ((($served_period[0] / 12) * $employe->leave[0]->leave_accumulated) + ($served_period[1] / ($employe->leave[0]->working_date * 12)));
                    $incometaxencashedLeave = $this->getEncashedleaveincomeTax($employe->leave[0]->working_date, $net_accumulated_leave_day,
                        $salaries->total_salary, $served_period[1], $served_period[0], round($net_accumulated_leave_day));
                    $grossencashedleave = $this->getgrossEarnedencashedLeave($net_accumelated_leave,
                        $employe->leave[0]->working_date, $served_period[1], $employe->salary->last()->total_salary, round($net_accumulated_leave_day));
                    $grossencashedleaveBymonth = $this->getgrossEarnedencashedLeavebymonth($grossencashedleave, $served_period[0], $employe->salary->count());
                    $monthly_taxable_salary_with_encashed = $salaries->total_salary + $grossencashedleaveBymonth;
                    $incometax_salary_encashed_leave = $this->getincometaxVariableencasheleave($monthly_taxable_salary_with_encashed, $grossencashedleaveBymonth);
                    $incometax_salary_without_leave = $this->getincometaxVariableencasheleave($salaries->total_salary, $grossencashedleaveBymonth);
                    $monthlytax_encashed_leave = $incometax_salary_encashed_leave - $incometax_salary_without_leave;
                    $salaryArry[$s] = [
                        'salaryId' => $salaries->id,
                        'employeId' => $salaries->employe_id,
                        'taxable_salary' => $salaries->total_salary,
                        'salary_type' => $salaries->payment_type,
                        'gross_encashed_leave_bymonth' => $grossencashedleaveBymonth,
                        'taxable_salary_including_encashedleave' => $salaries->total_salary + $grossencashedleaveBymonth,
                        'incometax_encashed_leave' => $incometax_salary_encashed_leave,
                        'incometax_without_encashed_leave' => $incometax_salary_without_leave,
                        'monthlytax_encashed_leave' => $monthlytax_encashed_leave,
                        'encashe_leave_after_incometax' => $grossencashedleaveBymonth - $monthlytax_encashed_leave
                    ];
                    $s++;
                }
                $aggregateArray[$a] = [
                    'employe' => $employeArry,
                    'encashedLeave' => $salaryArry
                ];
                $a++;
            }
        }
        return $aggregateArray;

            }
    public function getfixedEncashedBycompany($company_id,$date){
      $from=Carbon::parse($date)->startOfMonth();
      $to =Carbon::parse($date)->endOfMonth();
        $emploies=EmployeInformation::has('salary')
        ->with(['salary'=> function($query){
            $query->where('payment_type','fixed');
        },'leave'=>function($que) use($from,$to){
            $que->whereBetween('issued_date',[$from,$to]);
        }])->has('leave')
            ->where('company_id',$company_id)->get();

        $e=0;
        $aggregateArray=array();
        $a=0;
        foreach($emploies as $employe){
            $salaryArry=array();
            $s=0;
            $employename=null;

            if(!(count($employe->salary)===0)) {
                foreach ($employe->salary as $salaries) {
                    if (!(count($employe->leave) === 0)) {

                        $served_period = explode('-', $employe->leave[0]->served_leaved_period);
                        $net_accumulated_leave_day = ((($served_period[0] / 12) * $employe->leave[0]->leave_accumulated) + ($served_period[1] / ($employe->leave[0]->working_date * 12)));
                        $grossencashedleave = $this->getgrossEarnedencashedLeave($net_accumulated_leave_day,
                            $employe->leave[0]->working_date, $served_period[1], $salaries->total_salary, round($net_accumulated_leave_day));
                        $incometaxencashedLeave = $this->getEncashedleaveincomeTax($employe->leave[0]->working_date, $net_accumulated_leave_day,
                            $salaries->total_salary, $served_period[1], $served_period[0], round($net_accumulated_leave_day));
                    }
                    $salaryArry[$s] = [
                        'employename' => $employe->full_name,
                        'salaryId' => $salaries->id,
                        'employeId' => $salaries->employe_id,
                        'taxable_salary' => $salaries->total_salary,
                        'salary_type' => $salaries->payment_type,
                        'annual_leave' => $employe->leave[0]->annual_leave,
                        'working_day' => $employe->leave[0]->working_date,
                        'encashed_leave_status'=>$employe->leave[0]->status,
                        'served_period' => $employe->leave[0]->served_leaved_period,
                        'accumulated_leave' => $employe->leave[0]->leave_accumulated,
                        'net_accumlated_leave_day' => $net_accumulated_leave_day,
                        'gross_encashed_leave' => $grossencashedleave,
                        'incometax_encashed_leave' => $incometaxencashedLeave,
                        'net_encashedleave_aftertax' => $grossencashedleave - $incometaxencashedLeave
                    ];
                    $s++;
                }

                $aggregateArray[$a] = [
                    'encashedLeave' => $salaryArry
                ];
                $a++;
            }
        }
        return $aggregateArray;

    }
    public function storeLeave($inputs){
        $i=0;
        $leaveArray[]=null;
        foreach($inputs->leave as $input){
            $leaveEnchased=new EncashedLeave();
            $leaveEnchased->id=Uuid::generate();
            $leaveEnchased->employe_id=$input['employe_id'];
            $leaveEnchased->leave_date=$input['leave_date'];
            $leaveEnchased->working_date=$input['working_date'];
            $leaveEnchased->annual_leave=$input['annual_leave'];
            $leaveEnchased->issued_date=$input['issued_date'];
            $leaveEnchased->status=0;
            $leaveEnchased->served_leaved_period=$input['served_leave_period'];
            $leaveEnchased->leave_accumulated=$input['leave_accumulated'];
            $leaveEnchased->save();
            $leaveArray[$i]=$leaveEnchased;
            $i++;

        }
      return $leaveArray;
    }
    public function updateLeave($inputs){
        $i=0;
        $leaveArray[]=null;
        foreach($inputs->leave as $input){
            $leaveEnchased=EncashedLeave::find($input['id']);
            $leaveEnchased->leave_date=$input['leave_date'];
            $leaveEnchased->working_date=$input['working_date'];
            $leaveEnchased->annual_leave=$input['annual_leave'];
            $leaveEnchased->issued_date=$input['issued_date'];
            $leaveEnchased->status=0;
            $leaveEnchased->served_leaved_period=$input['served_leave_period'];
            $leaveEnchased->leave_accumulated=$input['leave_accumulated'];
            $leaveEnchased->save();
            $leaveArray[$i]=$leaveEnchased;
            $i++;

        }
        return $leaveArray;


    }
    public function deleteLeave($id){
      $leave=EncashedLeave::find($id);
      $leave->delete();
      return $leave;
  }
}
