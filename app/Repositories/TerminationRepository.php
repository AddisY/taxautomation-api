<?php

namespace App\Repositories;

use App\Entities\EmployeInformation;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Termination;
use App\Validators\TerminationValidator;
use Webpatser\Uuid\Uuid;

;

/**
 * Class TerminationRepository
 * @package namespace App\Repositories;
 */
class TerminationRepository {
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Termination::class;
    }
   public function getAlltermination($company_id){
       $termination=EmployeInformation::where('company_id',$company_id)->get();
       $terminateArry=array();
       $i=0;
       foreach($termination as $terminate){
           if(count($terminate->termination)){
               $terminateArry[$i]=[
                   'termination'=>$terminate->termination
               ];
               $i++;
           }
       }
       return $terminateArry;
   }

    public  function getTermination($id){
        $termination=Termination::find($id);
        return $termination;
    }

    public function getBycompany($company_id){
        $employes=EmployeInformation::where('company_id')->get();
        $terminations[]=null;
        $i=0;
        foreach($employes as $employe){
           $terminations[$i]= $employe->termination();
            $i++;
        }
        return $terminations;

    }
    public  function storeTermination($inputs){
        $i=0;
        $terminations[]=null;
        foreach($inputs->termination as $input){
            $terminate=new Termination();
            $terminate->id=Uuid::generate();
            $terminate->employe_id=$input['employe_id'];
            $terminate->end_date=$input['end_date'];
            $terminate->type=$input['type'];
            $terminate->status=0;
            $terminate->save();
            $employe=EmployeInformation::find($input['employe_id']);
            $employe->status=0;
            $terminations[$i]=$terminate;
            $i++;
        }
        return $terminations;
    }
    public function updateTermination($inputs){
        $i=0;
        $terminations[]=null;
        foreach($inputs->termination as $input){
            $terminate=Termination::find($input['id']);
            $terminate->end_date=$input['end_date'];
            $terminate->type=$input['type'];
            $terminate->save();
            $terminations[$i]=$terminate;
            $i++;
        }
        return $terminations;

    }
    public function deleteTermination($id){
        $terminate=Termination::find($id);
        $employe= EmployeInformation::find($terminate->employe_id);
        $employe->status=1;
        $terminate->delete();
        return $terminate;
    }

    private function servarancePaybothcase($year,$month,$days,$salary,$type){
        $result=0;
        if($type===1){
            $duration=$year+$month+$days;
            if($duration>0){
                $result=($salary+($salary/3*($year*12)+$month)/12);
            }
        }elseif($type===2){
            $result=$salary*2;
        }
       return $result;
    }
    private function getservarancePayincometax($salary,$servarancepy_fullmonth,$total_servarancepay){
        $result2=0;
        $result=0;
        $servarance=($total_servarancepay/$salary-$servarancepy_fullmonth)*$salary;
        if($salary<=600) {
            $result2 = $salary*0;
        }
        elseif($salary<=1650 ){
            $result2=$salary*0.1-60;
        }elseif($salary<=3200) {
            $result2 = $salary * 0.15 - 142.50;
        }elseif($salary<=5250 ){
            $result2=$salary * 0.2 - 302.50;
        }elseif($salary<=7800){
            $result2=$salary *0.25 - 565;
        }elseif($salary<=10900 ){
            $result2=$salary * 0.3 - 955;
        }else {
            $result2 = $salary * 0.35 - 1500;
        }


        if($servarance<=600) {
            $result = $servarance*0;
        }
        elseif($servarance<=1650 ){
            $result=$servarance*0.1-60;
        }elseif($servarance<=3200) {
            $result = $servarance * 0.15 - 142.50;
        }elseif($servarance<=5250 ){
            $result=$servarance * 0.2 - 302.50;
        }elseif($servarance<=7800){
            $result=$servarance *0.25 - 565;
        }elseif($servarance<=10900 ){
            $result=$servarance * 0.3 - 955;
        }else {
            $result= $servarance * 0.35 - 1500;
        }

       return ($result2*$servarancepy_fullmonth)+$result;
    }

    public function getseverancePaybycompany($company_id,$date){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $emploies=EmployeInformation::has('salary')
            ->with(['salary'
                ,'termination'=>function ($query) use($from,$to){
                    $query->whereBetween('end_date',[$from,$to]);
                }])->has('termination')
            ->where('company_id',$company_id)->get();
             $d=array();
        $i=0;

        foreach($emploies as $employe){
            if(!($employe->termination===null)) {
                $start_date = $employe->start_date;
                $end_date = $employe->termination['end_date'];
                $served_year = abs(strtotime($start_date) - strtotime($end_date));
                $years = floor($served_year / (365 * 60 * 60 * 24));
                $months = floor(($served_year - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                $days = floor(($served_year - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                $servarancepay_byemployess = $this->servarancePaybothcase($years, $months, $days, $employe->salary->last()->total_salary, 1);
                $servarancepay_byemployers = $this->servarancePaybothcase($years, $months, $days, $employe->salary->last()->total_salary, 2);
                $total_servarancepay = $servarancepay_byemployess + $servarancepay_byemployers;
                $servarcncepay_fullmonth = round($total_servarancepay / $employe->salary->last()->total_salary);
                $servarancepay_incometax = $this->getservarancePayincometax($employe->salary->last()->total_salary, $servarcncepay_fullmonth, $total_servarancepay);
                $d[$i] = [
                    'id' => $employe->id,
                    'employe_name' => $employe->full_name,
                    'served_year' => $years . ',' . $months . ',' . $days,
                    'termination_type' => $employe->termination['type'],
                    'last_salary' => $employe->salary->last()->total_salary,
                    'servarancepay_byemployess' => $servarancepay_byemployess,
                    'servarancepay_byemployers' => $servarancepay_byemployers,
                    'total_servarancepay' => $total_servarancepay,
                    'servarance_pay_fullmonth' => $servarcncepay_fullmonth,
                    'servarance_pay_incometax' => $servarancepay_incometax,
                    'net_servarancepay' => $total_servarancepay - $servarancepay_incometax


                ];
                $i++;
            }
    }

        return $d;
    }

}
