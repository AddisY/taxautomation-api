<?php
/**
 * Created by PhpStorm.
 * User: Mohammed
 * Date: 3/24/2016
 * Time: 2:23 PM
 */

namespace App\Repositories;

use App\Entities\Users\User;
use Illuminate\Support\Facades\Hash;
use Webpatser\Uuid\Uuid;

class UserRepository
{
    public function getAllUsers($request, $isDropdown, $n)
    {
        $users = null;
        if ($isDropdown) {
            $users = User::latest()
                ->get();
        } else {
            $users = User::latest()
                //->userSearch($request)
                //->filterByStatus($request)
                ->paginate($n);
        }

        return $users;
    }

    public function getCurrentUser()
    {
        $user = app('Dingo\Api\Auth\Auth')->user();

        return User::find($user->id);
    }

    public function changePassword($user_id, $oldPassword, $newPassword)
    {
        if ($this->checkOldPasswordR($user_id, $oldPassword)) {
            $this->changePasswordR($user_id, $newPassword);
            return 'Password Change Successfully';
        } else {
            return 'Old Password InCorrect';
        }
    }

    public function getUserDetail($uid)
    {
        return User::find($uid);
    }

    public function storeUser($input)
    {
        $user = $this->storeUserR($input->user);

        return $user;
    }

    public function updateUser($input, $userId)
    {
        $user = $this->updateUserR($input->user, $userId);
        return $user;
    }

    public function destroyUser($id)
    {
        $user = User::find($id);
        $user->delete();
        return $user;
    }

    public function changeStatus($userId, $status)
    {
        $user = User::find($userId);
        $user->update([
            'status' => $status === User::STATUS_ACTIVE ? User::STATUS_ACTIVE : User::STATUS_INACTIVE
        ]);

        return $user;
    }

    private function storeUserR($input)
    {
        $id = Uuid::generate(4);
        User::create([
            'id' => $id,
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'user_type' => $input['user_type']
        ]);

        return User::find($id);
    }

    private function updateUserR($input, $userId)
    {
        $user = User::find($userId);
        $user->update([
            'name' => $input['name']
        ]);

        return $user;
    }

    private function checkOldPasswordR($user_id, $password)
    {
        $oldPassword = crypt((string)($password),User::find($user_id)->password);

        if($oldPassword === User::find($user_id)->password){
            return true;
        }
        else {
            return false;
        }
    }

    private function changePasswordR($userId, $newPassword)
    {
        $user = User::find($userId);
        $user->update([
            'password' => Hash::make((string)$newPassword)
        ]);

        return $user;
    }
}