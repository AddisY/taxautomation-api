<?php

namespace App\Repositories;

use App\Entities\Bonus;
use App\Entities\EncashedLeave;
use App\Entities\OfficeExpenses;
use App\Entities\OverTime;
use App\Entities\Salary;
use App\Entities\Termination;
use App\Entities\Users\User;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\EmployeInformation;
use App\Validators\EmployeInformationValidator;
use Webpatser\Uuid\Uuid;



/**
 * Class EmployeInformationRepositoryEloquent
 * @package namespace App\Repositories;
 */
class EmployeInformationRepository extends SalaryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmployeInformation::class;
    }
   public function getallEmploye($per_page){
       $employeInfo=EmployeInformation::paginate($per_page);
       return $employeInfo;
   }
    public function getEmployebydate($company_id,$date){
            $from=Carbon::parse($date)->startOfMonth();
            $to =Carbon::parse($date)->endOfMonth();
            $employeInfo= EmployeInformation::where('company_id',$company_id)
                ->whereBetween('start_date',[$from,$to]);
            return $employeInfo;



    }
    public  function  getEmployeBycompany($company_id){
        $employe=EmployeInformation::where('company_id',$company_id)->get();
        return $employe;
    }
    public function getemployeeInfo($id)
    {
        $employeInfo = EmployeInformation::with('salary')
            ->with('overtime')
            ->with('termination')
            ->with('allowance')
            ->with('bonus')
            ->with('leave')
            ->where('id',$id)->get();
        return $employeInfo;
    }
    public function getemployeLeave($id,$date)
    {
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $employeInfo = EmployeInformation::with(['leave'=>function($query) use($from,$to){
        $query->whereBetween('issued_date',[$from,$to]);}])->where('id',$id)->get();
        return $employeInfo;
    }
    private function overtimeCalculation($start_time,$end_time,$ot_date){
        $start = explode(':', $start_time);
        $end = explode(':', $end_time);

         $total_hours =($end[0]-$start[0]).':'.($end[1]-$start[1]);

//        if($end[0]===$start[0]){
//            $total_hours= ($end[0]-$start[0]).':'.($end[1]-$start[1]);
//        }
//        elseif($end[1]===$start[1]){
//            $total_hours=$end[0]-$start[0].':'.$end[1]-$start[1];
//        }else{
//            $total_hours =($end[0]-$start[0]).':'.($end[1]-$start[1]);
//        }

        $hours = explode(':',$total_hours);

        if(intval($start[0])<22 && intval($start[0])>5 ){
            $performance= (intval($hours[0])*1.25 )+((intval($hours[1])/60)*1.25);

        }elseif(intval($start[0])>22 && intval($start[0])<=5){
            $performance= (intval($hours[0])*1.50 )+((intval($hours[1])/60)*1.50);
        }elseif($this->isWeekend($ot_date)){
            $performance = (intval($hours[0])*2.00 )+((intval($hours[1])/60)*2.00);
        }elseif($this->isPublicHoliday($ot_date)){
            $performance= (intval($hours[0])*2.50 )+((intval($hours[1])/60)*2.50);
        }else{
            $performance=0;
        }

        return $performance;
    }
    private  function isPublicHoliday($date){

        $month=intval(date("m", strtotime($date)));
        $day=intval(date("m", strtotime($date)));
        if($month===1 &&($day===7||$day=19)){
            $result = true;
        }elseif($month===3 &&($day===2||$day=20)){
            $result=true;

        }elseif($month===4 &&($day===16||$day=14)){
            $result=true;
        }elseif($month===5 &&($day===1||$day=5 || $day===28)){
            $result =true;

        }elseif($month===6 &&($day===21||$day=26)){
            $result=true;

        }elseif($month===9 &&($day===2||$day===11||$day===22||$day===27)){
            $result = true;
        }elseif($month===11 &&($day===1||$day=21)){
            $result=true;
        }else{
            $result=false;
        }

        return $result;

    }
    private function isWeekend($date) {
        return (date('N', strtotime($date)) >= 6);
    }
    private function gettaxableTransportallowance($transportAllowance,$salary){
        $result=0;

        if($transportAllowance>2200 || $transportAllowance>$salary*0.25){
            $result=$transportAllowance-2200;

        }
        if( $transportAllowance>$salary*0.25){
            $result=$transportAllowance-$salary*0.25;

        }
       return $result;


    }
    private  function getincomeTax($salary){
        $result=0;
        if($salary<=600) {
            $result = $salary*0;
        }
        elseif($salary<=1650 ){
            $result=$salary*0.1-60;
        }elseif($salary<=3200) {
            $result = $salary * 0.15 - 142.50;
        }elseif($salary<=5250 ){
            $result=$salary * 0.2 - 302.50;
        }elseif($salary<=7800){
            $result=$salary *0.25 - 565;
        }elseif($salary<=10900 ){
            $result=$salary * 0.3 - 955;
        }elseif($salary>10900) {
            $result = $salary * 0.35 - 1500;
        }
        return $result;
        
    }
    public function getpensionReport($company_id,$date){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $user=User::where('uid',$company_id)->first();
        $emploies= EmployeInformation::with(['salary'=>function($query) use($from,$to){
            $query->whereBetween('pay_day',[$from,$to]);},
            'bonus'=>function($que) use($from,$to) {
        $que->whereBetween('pay_day', [$from, $to]);},
            'overtime'=>function($q) use($from,$to){
                $q->whereBetween('overtime_date',[$from,$to]);
            },
            'termination'=>function($q) use($from,$to){
                $q->whereBetween('end_date',[$from,$to]);
            },'allowance'=>function($qu) use($from,$to){
                $qu->whereBetween('end',[$from,$to]);
            },'leave'=>function($qur) use($from,$to){
                $qur->whereBetween('issued_date',[$from,$to]);
            } ])->where('company_id',$company_id)->get();
        $officeexpses = OfficeExpenses::where('company_id',$company_id)->get();
        $o=0;
        $employearry=array();
        $terminated=array();
        $officeArray=array();
        $total_salary=0;
        $t=0;
        $i=0;
        foreach($officeexpses as $officeexp){
            $officeArray[$o]=[
                'issued_date'=>$officeexp->issue_date,
                'bill_code'=>$officeexp->bill_code,
                'total_price'=>$officeexp->total_price,
            ];
            $o++;

        }


        foreach($emploies as $employe){
            if(!($employe->termination===null)){

                $terminated[$t]=[
                    'fullname'=>$employe->full_name,
                    'tin_number'=>$employe->tin_number,
                ];
            }

          if (!(count($employe->salary)===0)){
              $employearry[$i]=[
                  'full_name'=>$employe->full_name,
                  'tin_number'=>$employe->tin_number,
                  'salary'=>$employe->salary->last()->total_salary,
                  'start_date'=>$employe->start_date,
                  'employe_contribution'=>($employe->salary->last()->total_salary)*0.07,
                  'employyer_contribution'=>($employe->salary->last()->total_salary)*0.11,
                  'total_contribution'=>(($employe->salary->last()->total_salary)*0.07)+(($employe->salary->last()->total_salary)*0.11),
                  $total_salary= $total_salary +$employe->salary->last()->total_salary

              ];
              $i++;

          }


        }
        $pension=[
            'company_Info'=>$user,
            'employe_info'=>$employearry,
            'numberof_employe'=>count($employearry),
            'total_salary'=>$total_salary,
            'terminated'=>$terminated,
            'office_exp'=>$officeArray
        ];
        return $pension;


    }
    public function getpayrollReport($company_id,$date)
    {
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $user =User::where('uid',$company_id)->first();
        $emploies= EmployeInformation::with(['salary'=>function($query) use($from,$to){
            $query->whereBetween('pay_day',[$from,$to]);},
            'bonus'=>function($que) use($from,$to) {
        $que->whereBetween('pay_day', [$from, $to]);
    }, 'overtime'=>function($q) use($from,$to){
        $q->whereBetween('overtime_date',[$from,$to]);
        },'termination'=>function($q) use($from,$to){
        $q->whereBetween('end_date',[$from,$to]);
    },'allowance'=>function($qu) use($from,$to){
            $qu->whereBetween('end',[$from,$to]);
        },'leave'=>function($qur) use($from,$to){
                $qur->whereBetween('issued_date',[$from,$to]);
            }])->where('company_id',$company_id)->get();
      $officeexpses = OfficeExpenses::where('company_id',$company_id)
          ->whereBetween('issue_date',[$from,$to])
          ->get();
        $employearry=array();
        $i=0;
        $total_salary=0;
        $terminated=array();
        $officeArray=array();
        $o=0;
        $t=0;
        foreach($officeexpses as $officeexp){
            $officeArray[$o]=[
                'issued_date'=>$officeexp->issue_date,
                'bill_code'=>$officeexp->bill_code,
                'total_price'=>$officeexp->total_price,
            ];
           $o++;
        }
       foreach($emploies as $employe){
           if(!(count($employe->salary)===0)) {
               $overtimeArry = array();
               $o = 0;
               $transportallowance = 0;
               $otherallowance = 0;
               $taxble_transport_allowance = 0;
               $salary = 0;
               $overtime_amount = 0;
               foreach ($employe->overtime as $over) {
                  $overtime_amount = $this->overtimeCalculation($over->start_time, $over->end_time, $over->overtime_date);
               }
               if (!($employe->termination === null)) {
                   $terminated[$t] = [
                       'fullname' => $employe->full_name,
                       'tin_number' => $employe->tin_number,
                   ];
               }
               $salary = $employe->salary->last()->total_salary;
               if (!count($employe->allowance) == 0) {

                   $transportallowance1 = $employe->allowance->where('type', 't1')->sum('total_amount');
                   $transportallowance2 = $employe->allowance->where('type', 't2')->sum('total_amount');
                   $transportallowance=$transportallowance1+$transportallowance2;
                   $otherallowance = $employe->allowance->where('type', 'a1')->sum('total_amount');
                   $taxble_transport_allowance1 = $this->gettaxableTransportallowance($employe->allowance->where('type', 't2')->sum('total_amount'), $employe->salary->last()->total_salary);
                   $taxble_transport_allowance2 = $transportallowance2;
                   $taxble_transport_allowance= $taxble_transport_allowance1+$taxble_transport_allowance2;
               }
               $employearry[$i] = [
                   'employe_id' => $employe->id,
                   'employe_name' => $employe->full_name,
                   'tin_number' => $employe->full_name,
                   'start_date' => $employe->start_date,
                   'salary' => $salary,
                   'transportallowance' => $transportallowance,
                   'taxable_transport_allowance' => $taxble_transport_allowance,
                   'otherallowance' => $otherallowance,
                   'overtime' => $overtime_amount,
                   'taxable_income' => $salary,
                   'income_tax' => $this->getincomeTax($salary),
                   'cost_sharing' => $employe->cost_sharing,
                   'net_payment' => ($salary + $transportallowance + $otherallowance + $overtime_amount + $taxble_transport_allowance) - $this->getincomeTax($salary),
                   $total_salary = $total_salary + $salary
               ];
               $i++;
           }
            }

        $payroll=[
            'company_Info'=>$user,
            'employe_info'=>$employearry,
             'numberof_employe'=>count($employearry)==0?0:count($employearry),
            'total_salary'=>$total_salary,
            'terminated'=>$terminated,
            'office_exp'=>$officeArray
        ];
         return $payroll;
    }
    public function storeEmployeInfo($inputs){
        $employeInfo[]=null;
        $i=0;
        foreach($inputs->employeinfo as $input){
            $emp =$input["employe"];
            $employe= new EmployeInformation();
            $employe->id =$employe_id=Uuid::generate();
            $employe->full_name=$emp['full_name'];
            $employe->company_id=$emp['company_id'];
            $employe->tin_number=$emp['tin_number'];
            $employe->status=1;
            $employe->department=$emp['department'];
            $employe->start_date=$emp['start_date'];
            $employe->cost_sharing=$emp['cost_sharing'];
            $employe->save();
           $salary= $this->storeSalary($input,$employe_id);
            $employeInfo[$i]=[
                'employeInfo'=>$employe,
                'salary'=>$salary
            ];
            $i++;
        }
        return $employeInfo;
    }
    public function updateEmployeInfo($inputs){
        $employeInfo[]=null;
        $salaries[]=null;
        $i=0;
        $j=0;
        $salary=null;
        foreach($inputs->employeinfo as $input){
            $emp =$input["employe"];
            $employe= EmployeInformation::find($emp['id']);
            $employe->full_name=$emp['full_name'];
            $employe->tin_number=$emp['tin_number'];
            $employe->department=$emp['department'];
            $employe->start_date=$emp['start_date'];
            $employe->cost_sharing=$emp['cost_sharing'];
            $employe->save();
              foreach($input['salary'] as $salaryy) {
                  if($salaryy['id']===""){
                      $salary = new Salary();
                      $salary->id=Uuid::generate();
                      $salary->employe_id=$emp['id'];
                      $salary->total_salary = $salaryy['total_salary'];
                      $salary->payment_type = $salaryy['payment_type'];
                      $salary->duration = $salaryy['duration'];
                      $salary->save();
                  }else{
                      $salary = Salary::find($salaryy['id']);
                      $salary->total_salary = $salaryy['total_salary'];
                      $salary->payment_type = $salaryy['payment_type'];
                      $salary->duration = $salaryy['duration'];
                      $salary->save();
                  }


                $salaries[$j] = $salary;
                  $j++;
            }
            $employeInfo[$i]=[
                'employeInfo'=>$employe,
                'salary'=>$salary
            ];
            $i++;
        }
        return $employeInfo;
    }
//    public function updateSalary($input){
//        $salary=Salary::find($id);
//        $salary->total_salary = $input->salary['total_salary'];
//        $salary->payment_type = $input->salary['payment_type'];
//        $salary->duration = $input->salary['duration'];
//        $salary->save();
//        return $salary;
//
//    }
    public function deleteEmploye($id){
        $employe=EmployeInformation::find($id);
        $employe->delete();
        return $employe;
    }
    public function updatestatus($company_id,$date){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $employeinfo=EmployeInformation::where('company_id',$company_id)->get();
        $overtimearry=array();
        $leaveArray=array();
        $bonusArray=array();
        $terminateArray=array();
        $salaryArray=array();
        $o=0;
        $l=0;
        $b=0;
        $t=0;
        $s=0;

        foreach($employeinfo as $employe){
            $overtimes=OverTime::where('employe_id',$employe->id)->whereBetween('overtime_date',[$from,$to])->get();
            if(!(count($overtimes)===0)){
                foreach($overtimes as $overtime){
                    $overtime->status=1;
                    $overtime->save();
                    $overtimearry[$o]=[
                        'overtime'=>$overtime
                    ];
                    $o++;
                }
            }

            $leaves=EncashedLeave::where('employe_id',$employe->id)->whereBetween('issued_date',[$from,$to])->get();
            if(!(count($leaves)===0)){
                foreach($leaves as $leave){
                    $leave->status=1;
                    $leave->save();
                    $leaveArray[$l]=[
                        'leave'=>$leave
                    ];
                    $l++;
                }
            }
            $bonuses=Bonus::where('employe_id',$employe->id)->whereBetween('pay_day',[$from,$to])->get();
            if(!(count($bonuses)===0)){
                foreach($bonuses as $bonus){
                    $bonus->status=1;
                    $bonus->save();
                   $bonusArrayn[$b]=[
                        'bonus'=>$bonus
                    ];
                    $b++;
                }

            }
//            $salaries=Bonus::where('employe_id',$employe->id)->whereBetween('pay_day',[$from,$to])->get();
//            if(!(count($salaries)===0)){
//                foreach($salaries as $salary){
//                    $salary->status=1;
//                    $salary->save();
//                   $salaryArray[$s]=[
//                        'salary'=>$salary
//                    ];
//                    $s++;
//                }
//
//            }

            $termination=Termination::where('employe_id',$employe->id)->whereBetween('end_date',[$from,$to])->get();
            if(!(count($termination)===0)){
                foreach($termination as $terminate){
                    $terminate->status=1;
                    $terminate->save();
                    $terminateArray[$t]=[
                        'termination'=>$terminate
                    ];
                    $t++;
                }
            }

        }



    }


}
