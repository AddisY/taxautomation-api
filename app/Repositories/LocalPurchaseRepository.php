<?php

namespace App\Repositories;

use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\LocalPurchase;
use App\Validators\LocalPurchaseValidator;
use Webpatser\Uuid\Uuid;

;

/**
 * Class LocalPurchaseRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LocalPurchaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LocalPurchase::class;
    }
    public function getAlllocalpurchase($per_page){

        $loclPurchase= LocalPurchase::paginate($per_page);

         return $loclPurchase;
    }
    public function getlocalpurchaseBydate($date,$uid){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();

        $localPurchase= LocalPurchase::where('company_id',$uid)
            ->whereBetween('bill_date',[$from,$to]);
        return $localPurchase;

    }
    public function getbycompany($company_id){
        $localpurchase=LocalPurchase::where('company_id',$company_id)->get();
        return $localpurchase;
    }
    public function getlocalPurchase($id){
        $localPurchase = LocalPurchase::find($id);
        return $localPurchase;
    }
    public  function  getlocalPurchaseSummery($date){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $localgoodspurchase =LocalPurchase::where('purchase_type','goods')->whereBetween('bill_date',[$from,$to])->sum('item_price');
        $localservicepurchase =LocalPurchase::where('purchase_type','service')->whereBetween('bill_date',[$from,$to])->sum('item_price');

    }

    public function storelocalPurchase($inputs){
        $localPurchases[]=null;
        $i=0;
        foreach($inputs->locapurchase as $input) {
            $localPurchase = new LocalPurchase;
            $localPurchase->id = Uuid::generate();
            $localPurchase->company_name = $input['company_name'];
            $localPurchase->company_id = $input['company_id'];
            $localPurchase->item_price = $input['item_price'];
            $localPurchase->item_type = $input['item_type'];
            $localPurchase->tin_number = $input['tin_number'];
            $localPurchase->status = 0;
            $localPurchase->withholding_tax_number = $input['withholding_tax_number'];
            $localPurchase->purchase_type = $input['purchase_type'];
            $localPurchase->mrc_code = $input['mrc_code'];
            $localPurchase->bill_code = $input['bill_code'];
            $localPurchase->item_name = $input['item_name'];
            $localPurchase->bill_date = $input['bill_date'];
            $localPurchase->withholding_date = $input['withholding_date'];
            $localPurchase->withholding_code = $input['withholding_code'];
            $localPurchase->save();
            $localPurchases[$i]=$localPurchase;
            $i++;
        }
        return $localPurchases;
   }
  public function updatelocalPurchase($inputs){

      $localPurchases[]=null;
      $i=0;

      foreach($inputs->locapurchase as $input) {

          $localPurchase = LocalPurchase::find($input["id"]);
          $localPurchase->company_name = $input['company_name'];
          $localPurchase->item_price = $input['item_price'];
          $localPurchase->item_type = $input['item_type'];
          $localPurchase->tin_number = $input['tin_number'];
          $localPurchase->status =0;
          $localPurchase->withholding_tax_number = $input['withholding_tax_number'];
          $localPurchase->purchase_type = $input['purchase_type'];
          $localPurchase->mrc_code = $input['mrc_code'];
          $localPurchase->bill_code = $input['bill_code'];
          $localPurchase->item_name = $input['item_name'];
          $localPurchase->bill_date = $input['bill_date'];
          $localPurchase->withholding_date = $input['withholding_date'];
          $localPurchase->withholding_code = $input['withholding_code'];
          $localPurchase->save();
          $localPurchases[$i]=$localPurchase;
          $i++;
      }
      return $localPurchases;

  }
    public function deletelocalPurchase($ids){
        $localPurchasearray=array();
        $i=0;
        foreach($ids->localpurchase as $id)
        {
            $localPurchase=LocalPurchase::find($id['id']);
            $localPurchase->delete();
            $localPurchasearray[$i]=[
                'localpurchase'=>$localPurchase
            ];

        }

        return $localPurchasearray;


    }



}
