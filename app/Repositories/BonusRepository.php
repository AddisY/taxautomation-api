<?php

namespace App\Repositories;

use App\Entities\EmployeInformation;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Bonus;
use App\Validators\BonusValidator;
use Webpatser\Uuid\Uuid;

;

/**
 * Class BonusRepository
 * @package namespace App\Repositories;
 */
class BonusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Bonus::class;
    }
    public function getAllbonus($company_id){
        $bonuses=EmployeInformation::where('company_id',$company_id)->get();
        $bonusArray=array();
        $i=0;
        foreach($bonuses as $bonus){
            if(count($bonus->bonus)){
                $bonusArray[$i]=[
                    'bonus'=>$bonus->bonus
                ];
                $i++;
            }

        }
        return $bonusArray;
    }
    public function getBonus($id){
        $bonus=Bonus::find($id);
        return $bonus;
    }
    public function storeBonus($inputs){
        $i=0;
        $bonuses[]=null;
        foreach($inputs->bonus as $input){
            $bonus= new Bonus();
            $bonus->id=Uuid::generate();
            $bonus->employe_id=$input['employe_id'];
            $bonus->pay_day=$input['pay_day'];
            $bonus->status=0;
            $bonus->served_leaved_period=$input['served_leaved_period'];
            $bonus->total_amount=$input['total_amount'];
            $bonus->save();
            $bonuses[$i]=$bonus;
            $i++;
        }
        return $bonuses;

    }
    private function annualBonus($salary,$bonus,$served_month){
        $result=0;
        $value=($salary+($bonus/$served_month));
        if($value<=600) {
            $result = ($salary + ($bonus / $served_month)) * 0;
        }
            elseif($value<=1650){
                $result=$value*0.1-60;
            }elseif($value<=3200) {
            $result = ($salary + ($bonus / $served_month)) * 0.15 - 142.50;
        }elseif($value<=5250){
            $result=$value * 0.2 - 302.50;
        }elseif($value<=7800){
            $result=$value *0.25 - 565;
        }elseif($value<=10900){
            $result=$value * 0.3 - 955;
        }elseif($value>10900){
            $result=$value* 0.35 - 1500;
        }
        return $result;

    }

    public function getfixedBonusBycompany($company_id,$date){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $emploies=EmployeInformation::
            with(['bonus'=>function($que) use($from,$to){
            $que->whereBetween('pay_day',[$from,$to]);
        }
            ,'salary'=>function ($query){
            $query->where('payment_type','fixed');
        }])
            ->where('company_id',$company_id)
            ->get();
        $employarry=array();
        $aggrigateBonus=array();
        $l=0;
        $i=0;
        $b=0;

        foreach($emploies as $employe){

            $salaryBonus=array();
            $e=array();
            $j=0;



        $employarry=[
                'employeID'=>$employe->id,
                'employeName'=>$employe->full_name,

                ];


             foreach($employe->salary as $salaries){
                if(!($employe->bonus===null )){
                    $server_period=explode('-',$employe->bonus[0]->served_leaved_period);
                    $annual_tax=$this->annualBonus($salaries->total_salary,$employe->bonus[0]->total_amount,$server_period[0]);
                    $salaryBonus[$j]=[
                        'id'=>$salaries->id,
                        'employe_id'=>$salaries->employe_id,
                        'total_salary'=>$salaries->total_salary,
                        'bonus_amount'=>$employe->bonus[0]->total_amount,
                        'serverd_period_month'=>$server_period[0],
                        'serverd_period_day'=>$server_period[1],
                        'annual_tax_bonus'=>$annual_tax,
                        'pay_day'=>$employe->bonus[0]->pay_day,
                       'net_bonus'=>($salaries->total_salary)-$annual_tax
                    ];
                    $j++;
                }
            }
                if(!(count($salaryBonus)==0)){
                    $aggrigateBonus[$b]=[
                        'employe'=>$employarry,
                        'bonus'=>$salaryBonus,
                    ];
                    $b++;
                }
            $i++;

        }
        return $aggrigateBonus;


    }


    private function getvariablebonus($annualBonus,$served_month,$numberofmonth){
        $result=0;
        $condition=($annualBonus/$served_month)*$numberofmonth;
        if($condition<=$annualBonus){
            $result = $annualBonus/$served_month;
        }
        return $result;
    }
    private  function getincometaxVariablebonus($salarybonus,$bonus){
        $value =$salarybonus;
        $result=0;
        if($value<=600 && $bonus>0) {
            $result = $salarybonus*0;
        }
        elseif($value<=1650 && $bonus>0){
            $result=$salarybonus*0.1-60;
        }elseif($value<=3200  && $bonus>0) {
            $result = $salarybonus * 0.15 - 142.50;
        }elseif($value<=5250 && $bonus>0){
            $result=$salarybonus * 0.2 - 302.50;
        }elseif($value<=7800  && $bonus>0){
            $result=$salarybonus *0.25 - 565;
        }elseif($value<=10900  && $bonus>0){
            $result=$salarybonus * 0.3 - 955;
        }elseif($value>10900  && $bonus>0){
            $result=$salarybonus* 0.35 - 1500;
        }elseif($bonus<=0){
            $result=0;
        }
        return $result;
    }



    public function getvariableBonusbycompany($company_id,$date){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $emploies=EmployeInformation::
        with(['bonus'=>function($que) use($from,$to){
            $que->whereBetween('pay_day',[$from,$to]);
        }
            ,'salary'=>function ($query){
                $query->where('payment_type','variable');
            }])
            ->where('company_id',$company_id)
            ->get();


         $employearray=array();
         $bonusAggregation=array();
         $b=0;
         $e=0;
        $monthsum=0;
        $daysum=0;

        foreach($emploies as $employe){
            $salaryArray=array();
            $employearray=array();
            $s=0;
            if(!(count($employe->bonus)==0|| count($employe->salary)==0)){
                $employearray=[
                    'employe_id'=>$employe->id,
                    'full_name'=>$employe->full_name,
                    'bonus_for_year'=>$employe->bonus[0]->total_amount,
                    'served_period'=>$employe->bonus[0]->served_leaved_period
                ];
                $served_day =explode('-',$employe->bonus[0]->served_leaved_period);
                $monthsum =  intval($served_day[0]);
                $daysum=  intval($served_day[1]);
                foreach($employe->salary as $salaries ){
                $bonus=$this->getincometaxVariablebonus($employe->bonus[0]->total_amount,$monthsum,$employe->salary->count());
                $salarywithTax=$this->getincometaxVariablebonus($salaries->total_salary,$employe->bonus[0]->total_amount);
                $salaryArray[$s]=[
                    'id'=>$salaries->id,
                    'served_period'=>$employe->bonus[0]->served_leaved_period,
                    'employe_id'=>$salaries->employe_id,
                    'salary'=>$salaries->total_salary,
                    'month'=>$salaries->duration,
                    'anual_bonus'=>$employe->bonus[0]->total_amount,
                    'bonus'=>$bonus,
                    'taxable_income_with_bonus'=>$bonus+$salaries->total_salary,
                   'taxed_income_without_bonus'=>$salarywithTax,
                   'monthly_tax_on_tax'=>$bonus - $salarywithTax
                ];
                $s++;

           }

            $bonusAggregation[$b]=[

                'employe'=>$employearray,
                'salary'=>$salaryArray
            ];
            $b++;
            }

        }
        return $bonusAggregation;

    }
    public function updateBonus($inputs)
    {
        $i=0;
       $bonuses[]=null;
        foreach($inputs->bonus as $input){
            $bonus=Bonus::find($input['id']);
            $bonus->total_amount=$input['total_amount'];
            $bonus->pay_day=$input['pay_day'];
            $bonus->served_leaved_period=$input['served_leaved_period'];
            $bonus->save();
            $bonuses[$i]=$bonus;
            $i++;
        }
        return $bonuses;
    }
    public function deleteBonus($id){
        $bonus=Bonus::find($id);
        $bonus->delete();
        return $bonus;
    }

}
