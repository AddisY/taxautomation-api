<?php

namespace App\Repositories;


use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Salary;
use App\Validators\SalaryValidator;
use Webpatser\Uuid\Uuid;

;

/**
 * Class SalaryRepository
 * @package namespace App\Repositories;
 */
class SalaryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Salary::class;
    }
    public function getAllsalaries(){
        $salary=Salary::all();
        return $salary;
    }
    public function getsalary($id){
        $salary=Salary::find($id);
        return $salary;
    }
    public function storeSalary($inputs,$employe_id){
        $salaries[]=null;
        $i=0;
        $salaries=$inputs["salary"];
       foreach($salaries as $salaryy) {
           $salary = new Salary();
           $salary->id = Uuid::generate();
           $salary->total_salary = $salaryy['total_salary'];
           $salary->payment_type = $salaryy['payment_type'];
           $salary->duration = $salaryy['duration'];
           $salary->pay_day=$salaryy['pay_day'];
           $salary->employe_id = $employe_id;
           $salary->save();
           $salaries[$i] = $salary;
       }
        return $salaries;
    }
    public function updateSalary($inputs){
        $salaries[]=null;
        $i=0;
        $salaries=$inputs;
    foreach($salaries['salary'] as $salaryy) {
            $salary = Salary::find($salaryy['id']);
            $salary->total_salary = $salaryy['total_salary'];
            $salary->payment_type = $salaryy['payment_type'];
            $salary->duration = $salaryy['duration'];
            $salary->pay_day = $salaryy['pay_day'];
            $salary->save();
            $salaries[$i] = $salary;
        }

        return $salaries;
    }
    public  function deleteSalary($salary_id){
        $salary=Salary::find($salary_id);
        $salary->delete();
        return $salary;
    }



}
