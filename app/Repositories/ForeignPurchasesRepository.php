<?php

namespace App\Repositories;

use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\ForeignPurchases;
use Webpatser\Uuid\Uuid;


/**
 * Class ForeignPurchasesRepository
 * @package namespace App\Repositories;
 */
class ForeignPurchasesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ForeignPurchases::class;
    }
   public function getallforeignPurchase($per_page){
       $foreignPurchase= ForeignPurchases::paginate($per_page);
       return $foreignPurchase;
   }
    public function getforeignpurchaseBydate($date,$uid){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $foreignPurchase=ForeignPurchases::where('company_id',$uid)
            ->whereBetween('dec_date',[$from,$to]);
        return $foreignPurchase;

    }

    public  function getbyCompany($company_id){
        $foreignPurchase=ForeignPurchases::where('company_id',$company_id)->get();
        return $foreignPurchase;
    }
    public function getforeignPurchase($id){
        $foreignPurchase=ForeignPurchases::find($id);
        return $foreignPurchase;
    }
    public function storeforeignPurchase($inputs){
        $foreignPurchases[]=null;
        $i=0;
        foreach($inputs->foreignpurchase as $input) {
            $foreignPurchase = new ForeignPurchases();
            $foreignPurchase->id = Uuid::generate();
            $foreignPurchase->dec_number = $input['dec_number'];
            $foreignPurchase->company_id = $input['company_id'];
            $foreignPurchase->dec_date = $input['dec_date'];
            $foreignPurchase->item_price = $input['item_price'];
            $foreignPurchase->status =0;
            $foreignPurchase->bill_code = $input['bill_code'];
            $foreignPurchase->tax_price = $input['tax_price'];
            $foreignPurchase->item_name = $input['item_name'];
            $foreignPurchase->save();
            $foreignPurchases[$i]=$foreignPurchase;
            $i++;
        }
        return $foreignPurchases;
    }

    public function updateforeignPurchase($inputs){
        $foreignPurchases[]=null;
        $i=0;
        foreach($inputs->foreignpurchase as $input) {
            $foreignPurchase = ForeignPurchases::find($input['id']);
            $foreignPurchase->dec_number = $input['dec_number'];
            $foreignPurchase->dec_date = $input['dec_date'];
            $foreignPurchase->item_price = $input['item_price'];
            $foreignPurchase->status =0;
            $foreignPurchase->bill_code = $input['bill_code'];
            $foreignPurchase->tax_price = $input['tax_price'];
            $foreignPurchase->item_name = $input['item_name'];
            $foreignPurchase->save();
            $foreignPurchases[$i]=$foreignPurchase;
            $i++;
        }
        return $foreignPurchases;
    }
    public function deleteforeignPurchase($ids){
        $foreignPurchasearray=array();
        $i=0;
        foreach($ids->foreignPurchase as $id){
            $foreignPurchase=ForeignPurchases::find($id['id']);
            $foreignPurchase->delete();
            $foreignPurchasearray[$i]=[
                'foreignpurchase'=>$foreignPurchase
            ];
            $i++;
        }

        return $foreignPurchasearray;
    }


}
