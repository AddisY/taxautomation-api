<?php

namespace App\Repositories;

use App\Entities\ForeignPurchases;
use App\Entities\LocalPurchase;
use App\Entities\LocalSales;
use App\Entities\NonRegisteredPurchase;
use App\Entities\Users\User;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;




/**
 * Class VATReportRepository
 * @package namespace App\Repositories;
 */
class VATReportRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
   public function getvatReport($date,$company_id){
       $vat_report= new User();
       $from=Carbon::parse($date)->startOfMonth();
       $to =Carbon::parse($date)->endOfMonth();
       $localgoodspurchase =LocalPurchase::where('company_id',$company_id)->where('purchase_type','product')->whereBetween('bill_date',[$from,$to])->sum('item_price');
       $localgoodspurchase_status =LocalPurchase::where('company_id',$company_id)->where('purchase_type','product')->whereBetween('bill_date',[$from,$to])->avg('status');
      $vat_report->localgoodspurchase =LocalPurchase::where('company_id',$company_id)->where('purchase_type','product')->whereBetween('bill_date',[$from,$to])->sum('item_price');
       $vat_report->localgoodspurchase_status =LocalPurchase::where('company_id',$company_id)->where('purchase_type','product')->whereBetween('bill_date',[$from,$to])->avg('status');
       $localgoodspurchase_vat=$localgoodspurchase*0.15;
       $vat_report->localgoodspurchase_vat=$localgoodspurchase*0.15;
       $localservicepurchase =LocalPurchase::where('company_id',$company_id)->where('purchase_type','service')->whereBetween('bill_date',[$from,$to])->sum('item_price');
       $localservicepurchase_status =LocalPurchase::where('company_id',$company_id)->where('purchase_type','service')->whereBetween('bill_date',[$from,$to])->avg('status');

        $vat_report->localservicepurchase =LocalPurchase::where('company_id',$company_id)->where('purchase_type','service')->whereBetween('bill_date',[$from,$to])->sum('item_price');
        $vat_report->localservicepurchase_satatus =LocalPurchase::where('company_id',$company_id)->where('purchase_type','service')->whereBetween('bill_date',[$from,$to])->avg('status');
       $localservicespurchase_vat=$localservicepurchase*0.15;
       $vat_report->localservicespurchase_vat=$localservicepurchase*0.15;
       $foreignPurchaseSummery=ForeignPurchases::where('company_id',$company_id)->whereBetween('dec_date',[$from,$to])->sum('tax_price');
       $foreignPurchaseSummery_status=ForeignPurchases::where('company_id',$company_id)->whereBetween('dec_date',[$from,$to])->avg('status');
       $vat_report->foreignPurchaseSummery=ForeignPurchases::where('company_id',$company_id)->whereBetween('dec_date',[$from,$to])->sum('tax_price');
       $vat_report->foreignPurchaseSummery_status=ForeignPurchases::where('company_id',$company_id)->whereBetween('dec_date',[$from,$to])->avg('status');;
       $foreignPurchaseSummery_vat =$foreignPurchaseSummery*0.15;
       $vat_report->foreignPurchaseSummery_vat = $foreignPurchaseSummery*0.15;
       $salessummery=LocalSales::where('company_id',$company_id)->whereBetween('bill_date',[$from,$to])->sum('item_price');
       $salessummery_status=LocalSales::where('company_id',$company_id)->whereBetween('bill_date',[$from,$to])->avg('status');;
       $vat_report->salessummery=LocalSales::where('company_id',$company_id)->whereBetween('bill_date',[$from,$to])->sum('item_price');
        $vat_report->salessummery_status=LocalSales::where('company_id',$company_id)->whereBetween('bill_date',[$from,$to])->avg('status');
       $salessummery_vat=$salessummery*0.15;
       $vat_report->salessummery_vat=$salessummery*0.15;
       $nontinPurchase=NonRegisteredPurchase::where('company_id',$company_id)->whereBetween('voucher_date',[$from,$to])->sum('item_price');
       $nontinPurchase_status=NonRegisteredPurchase::where('company_id',$company_id)->whereBetween('voucher_date',[$from,$to])->avg('status');
       $vat_report->nontinPurchase=NonRegisteredPurchase::where('company_id',$company_id)->whereBetween('voucher_date',[$from,$to])->sum('item_price');
       $vat_report->nontinPurchase_status=NonRegisteredPurchase::where('company_id',$company_id)->whereBetween('voucher_date',[$from,$to])->avg('status');
       $total_input_value= $localgoodspurchase+$localservicepurchase+$foreignPurchaseSummery+$nontinPurchase;
       $vat_report->total_input_value= $localgoodspurchase+$localservicepurchase+$foreignPurchaseSummery+$nontinPurchase;
       $taxable_total_input_value= $localgoodspurchase+$localservicepurchase+$foreignPurchaseSummery;
       $vat_report->taxable_total_input_value= $localgoodspurchase+$localservicepurchase+$foreignPurchaseSummery;
       $total_input_tax=$localgoodspurchase_vat+$localservicespurchase_vat+$salessummery_vat;
       $vat_report->total_input_tax=$localgoodspurchase_vat+$localservicespurchase_vat+$salessummery_vat;
       $vat_report->total_status=$localgoodspurchase_status===null?0:$localgoodspurchase_status+$localservicepurchase_status===null?0:$localservicepurchase_status
           +$foreignPurchaseSummery_status===null?0:$foreignPurchaseSummery_status+$nontinPurchase_status===null?0:$nontinPurchase_status;
         if ($salessummery_vat>=$total_input_tax){
             $net_vat_due=$salessummery_vat-$total_input_tax;
             $vat_report->net_vat_due=$salessummery_vat-$total_input_tax;
         }else{
             $net_vat_due=0;
             $vat_report->net_vat_due=0;
         }
       if($total_input_tax>=$salessummery){
           $vat_credit=$total_input_tax-$salessummery_vat;
           $vat_report->vat_credit=$total_input_tax-$salessummery_vat;
       }
       else{
           $vat_credit=0;
           $vat_report->vat_credit=0;








       }
       return $vat_report;
   }

    private  function calculatewithholding($type,$item_price){
        $result=0;
        if($type ==='service' && $item_price>3000)
            {
                $result=$item_price*0.02;
            }
        elseif($type==='product' && $item_price>10000){
            $result =$item_price*0.02;

            }

        return $result;

    }

    private function getotaltaxable($date,$company_id){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $totaloca=0;
       $totalnonreg= NonRegisteredPurchase::whereBetween('voucher_date',[$from,$to])->sum('item_price');
        $totalforeginPurchases = ForeignPurchases::where('company_id',$company_id)->whereBetween('dec_date',[$from,$to])->sum('item_price');
        $locaPurchases = LocalPurchase::where('company_id',$company_id)->whereBetween('withholding_date',[$from,$to])->get();
        foreach($locaPurchases as $locaPurchas){
            if($locaPurchas->purchase_type ==='service' && $locaPurchas->item_price>3000)
            {
              $totaloca = + $locaPurchas->item_price;

            }
            elseif($locaPurchas->purchase_type==='product' &&  $locaPurchas->item_price>10000){
                $totaloca = + $locaPurchas->item_price;


            }

        }
        return $totaloca;

    }

    public function getwithholdinglocalpurchaseReport($date,$company_id){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $locaPurchases = LocalPurchase::where('company_id',$company_id)->whereBetween('withholding_date',[$from,$to])->get();
        $localgoodspurchase_status =LocalPurchase::where('company_id',$company_id)->where('purchase_type','product')->whereBetween('bill_date',[$from,$to])->avg('status');
        $localservicepurchase_status =LocalPurchase::where('company_id',$company_id)->where('purchase_type','service')->whereBetween('bill_date',[$from,$to])->avg('status');
        $foreignPurchaseSummery_status=ForeignPurchases::where('company_id',$company_id)->whereBetween('dec_date',[$from,$to])->avg('status');
        $salessummery_status=LocalSales::where('company_id',$company_id)->whereBetween('bill_date',[$from,$to])->avg('status');
        $nontinPurchase_status=NonRegisteredPurchase::where('company_id',$company_id)->whereBetween('voucher_date',[$from,$to])->avg('status');
        $total_status=$localgoodspurchase_status===null?0:$localgoodspurchase_status+$localservicepurchase_status===null?0:$localservicepurchase_status
        +$foreignPurchaseSummery_status===null?0:$foreignPurchaseSummery_status+$nontinPurchase_status===null?0:$nontinPurchase_status;
        $foreginPurchases = ForeignPurchases::where('company_id',$company_id)->whereBetween('dec_date',[$from,$to])->get();
        $nonregisteredPurchases=NonRegisteredPurchase::where('company_id',$company_id)->whereBetween('voucher_date',[$from,$to])->get();
        $totalTaxableamount=ForeignPurchases::where('company_id',$company_id)->whereBetween('dec_date',[$from,$to])->sum('item_price')+
            NonRegisteredPurchase::whereBetween('voucher_date',[$from,$to])->sum('item_price')+$this->getotaltaxable($date,$company_id);
        $totalWithholdtax =($this->getotaltaxable($date,$company_id)*0.02) +
              (ForeignPurchases::where('company_id',$company_id)->whereBetween('dec_date',[$from,$to])->sum('item_price')*0.03)
           + (NonRegisteredPurchase::where('company_id',$company_id)->whereBetween('voucher_date',[$from,$to])->sum('item_price')*0.3);
        $reportss=[];
         $i=0;
           foreach($foreginPurchases as $foreginPurchase){
               $withholdingReport = new User();
               $withholdingReport->id=$foreginPurchase->id;
               $withholdingReport->tin_number='-';
               $withholdingReport->company_name = '-';
               $withholdingReport->zone = '-';
               $withholdingReport->sub_city = '-';
               $withholdingReport->woreda = '-';
               $withholdingReport->house_number = '-';
               $withholdingReport->item_price = $foreginPurchase->item_price;
               $withholdingReport->withholding = ($foreginPurchase->item_price) * 0.03;
               $withholdingReport->withholding_code = '-';
               $withholdingReport->withholding_date = $foreginPurchase->dec_date;
               $reportss[$i]=[
                   'withholdreport'=>$withholdingReport
               ];
               $i++;
           }
            foreach($locaPurchases as $localpurchas ){
                $withholdingReport = new User();
                $withholdingReport->id = $localpurchas->id;
                $withholdingReport->tin_number = $localpurchas->tin_number;
                $withholdingReport->company_name = $localpurchas->company_name;
                $withholdingReport->zone = '-';
                $withholdingReport->sub_city = '-';
                $withholdingReport->woreda = '-';
                $withholdingReport->house_number = '-';
                $withholdingReport->item_price = $localpurchas->item_price;
                $withholdingReport->withholding = $this->calculatewithholding($localpurchas->purchase_type,$localpurchas->item_price);
                $withholdingReport->withholding_code = $localpurchas->withholding_code;
                $withholdingReport->withholding_date = $localpurchas->withholding_date;
                $reportss[$i]=[
                    'withholdreport'=>$withholdingReport
                ];
                $i++;
            }

     foreach($nonregisteredPurchases as $nonregisteredPurchase)
        {
            $withholdingReport= new User();
            $withholdingReport->id =$nonregisteredPurchase->id;
            $withholdingReport->tin_number ='-';
            $withholdingReport->company_name = $nonregisteredPurchase->company_name;
            $withholdingReport->zone= $nonregisteredPurchase->zone;
            $withholdingReport->sub_city=$nonregisteredPurchase->sub_city;
            $withholdingReport->woreda =$nonregisteredPurchase->woreda;
            $withholdingReport->house_number=$nonregisteredPurchase->house_number;
            $withholdingReport->item_price=$nonregisteredPurchase->item_price;
            $withholdingReport->withholding=$nonregisteredPurchase->item_price*0.3;
            $withholdingReport->withholding_code = '-';
            $withholdingReport->withholding_date = $nonregisteredPurchase->voucher_date;
            $reportss[$i]=[
                'withholdReport'=>$withholdingReport
            ];
            $i++;
        }
        $aggregation= [
            'withholding'=>$reportss,
            'totalNumberWithholdees'=>$i,
            'totalTaxableAount'=>$totalTaxableamount,
            'totalWithholdTax'=>$totalWithholdtax,
            'withhold_status'=>$total_status
        ];
        return $aggregation;
    }

    public function updateAllstatus($date,$company_id){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();

        $localspurchases =LocalPurchase::where('company_id',$company_id)->whereBetween('bill_date',[$from,$to])->get();
        $locapurchasearry=array();
        $foreignpurchasearry=array();
        $salessummeryarry=array();
        $nontinpurchasearry=array();
        $i=0;$j=0;$l=0;$m=0;
        foreach($localspurchases as $localspurchase){
            $localspurchase->status=1;
            $localspurchase->save();
            $locapurchasearry[$i]=[
                'localpurchase'=>$localspurchase
            ];
            $i++;
        }
        $foreignPurchaseSummeries=ForeignPurchases::where('company_id',$company_id)->whereBetween('dec_date',[$from,$to])->get();
        foreach($foreignPurchaseSummeries as $foreignPurchaseSummery){
            $foreignPurchaseSummery->status=1;
            $foreignPurchaseSummery->save();
            $foreignpurchasearry[$j]=[
                'foreignpurchase'=>$foreignPurchaseSummery
            ];
            $j++;
        }
        $salessummeries=LocalSales::where('company_id',$company_id)->whereBetween('bill_date',[$from,$to])->get();
        foreach ($salessummeries as $salessummery) {
            $salessummery->status=1;
            $salessummery->save();
            $salessummeryarry[$l]=[
                'salessummery'=>$salessummery
            ];
            $l++;

        }
        $nontinPurchases=NonRegisteredPurchase::where('company_id',$company_id)->whereBetween('voucher_date',[$from,$to])->get();
        foreach($nontinPurchases as $nontinPurchase){
            $nontinPurchase->status=1;
            $nontinPurchase->save();
            $nontinpurchasearry[$m]=[
                'nontinpurchase'=>$nontinPurchase
            ];
        }

        $transaction=[
            'agg_localpurchase'=>$locapurchasearry,
            'agg_foreignpurchase'=>$foreignpurchasearry,
            'agg_salasesummery'=>$salessummeryarry,
            'agg_nontinpurchase'=>$nontinpurchasearry
        ];
        return $transaction;


    }


}
