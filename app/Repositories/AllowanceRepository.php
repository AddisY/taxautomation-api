<?php

namespace App\Repositories;

use App\Entities\EmployeInformation;
use Carbon\Carbon;

use DateTime;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Allowance;
use App\Validators\AllowanceValidator;
use Webpatser\Uuid\Uuid;

;

/**
 * Class AllowanceRepository
 * @package namespace App\Repositories;
 */
class AllowanceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Allowance::class;
    }
    public function getAllallowance($company_id){
        $allowances=EmployeInformation::where('company_id',$company_id)->get();
        $allowanceArray=array();
        $i=0;
        foreach($allowances as $allowance){
            if(count($allowance->allowance)){
                $allowanceArray[$i]=[
                    'allowance'=>$allowance->allowance
                ];
                $i++;
            }
        }
        return $allowanceArray;
    }
    public  function  getAllowance($id){
        $allowance= Allowance::find($id);
        return $allowance;
    }
    public function getallowanceByEmploye($company_id,$employe_id){
        $allowance=EmployeInformation::where('id',$employe_id)
            ->where('company_id',$company_id)
            ->allowance();
        return $allowance;

    }
    public function getAllowanceBycompany($company_id){

         $employes=EmployeInformation::with('allowance')
             ->has('allowance')
             -> where('company_id',$company_id)
             ->get();
        return $employes;

    }
    public function getAllowanceBydate($company_id,$date){
        $from=Carbon::parse($date)->startOfMonth();
        $to =Carbon::parse($date)->endOfMonth();
        $employes=EmployeInformation::with(['allowance'=>function($query) use($from,$to) {
             $query->whereBetween('end', [$from, $to]);
         }])
             ->has('allowance')
             ->has('salary')
             ->with('salary')
             -> where('company_id',$company_id)
             ->get();


        $allowanceArray=array();
        $i=0;


        foreach($employes as $employe){
            foreach($employe->allowance as $empallowance){
                $salary =$employe->salary->last()->total_salary;
                $start = new DateTime($empallowance->start);
                $end = new DateTime($empallowance->end);
                $interval = $start->diff($end)->format('%d');
             $allowanceArray[$i]=[
                    'employe_id'=>$empallowance->id,
                    'employe_id'=>$empallowance->employe_id,
                    'type'=>$empallowance->type,
                    'description' =>$empallowance->description,
                    'total_amount'=>$empallowance->total_amount,
                    'start'=>$empallowance->start,
                    'end'=>$empallowance->end,

                    'number_days'=>$interval,
                    'taxable_amount'=>$this->gettaxableTransportallowance($empallowance->total_amount,$salary,$empallowance->type,$empallowance->start,$empallowance->end),
                    'end'=>$empallowance->status,
                ];
                $i++;
            }


        }
        return $allowanceArray;

    }
    public function storeAllowance($inputs){
        $i=0;
        $allowances[]=null;
        foreach ($inputs->allowance as $input ) {
            $allowance= new Allowance();
            $allowance->id=Uuid::generate();
            $allowance->employe_id=$input['employe_id'];
            $allowance->type=$input['type'];
            $allowance->start=$input['start'];
            $allowance->end=$input['end'];
            $allowance->description=$input['description'];
            $allowance->status=0;
            $allowance->total_amount=$input['total_amount'];
            $allowance->save();
            $allowances[$i]=$allowance;
            $i++;
        }
        return $allowances;
    }
    public function updateAllowance($inputs){
        $i=0;
        $allowances[]=null;
        foreach ($inputs->allowance as $input ) {
            $allowance= Allowance::find($input['id']);
            $allowance->type=$input['type'];
            $allowance->total_amount=$input['total_amount'];
            $allowance->start=$input['start'];
            $allowance->end=$input['end'];
            $allowance->description=$input['description'];
            $allowance->save();
            $allowances[$i]=$allowance;
            $i++;
        }
        return $allowances;

    }



    private function gettaxableTransportallowance($allowance,$salary,$type,$start,$end){
        $result=0;
        $taxble=0;
        $startd = new DateTime($start);
        $endd = new DateTime($end);
        $interval = $startd->diff($endd)->format('%d');
        if($allowance<=255 && $type === 'Perdigm'){
            $result=0;
        }
        if($allowance<=0.04*$salary  && $type ==='Perdigm'){
            $result=$allowance;
        }
        if(($allowance>255 && $allowance > 0.04*$salary)&& $type ==='Perdigm'){
           $result = ($allowance-0.04*$salary)*$interval;
        }
        if($allowance>255 && $salary*0.04<255 && $type ==='Perdigm'){
             $result=($allowance-225)*$interval;
        }
        if($type ==='<25km from Home to Work'){
             $result = $allowance;
        }
        if($type ==='<25km from Work to Work' && ($allowance>2200 || $allowance>$salary*0.25)){
            $result=$allowance-2200;
        }
        if($type ==='<25km from Work to Work' && $allowance>$salary*0.25){
            $result=$allowance-$salary*0.25;
        }

        return $result;
    }
    public function deleteAllowance($id){
        $allowance=Allowance::find($id);
        $allowance->delete();
        return $allowance;
    }

}
