<?php

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->group(['prefix' => 'oauth'], function ($api) {
        $api->post('authorize', 'App\Http\Controllers\Auth\AuthController@authorizeClient');
    });

    // UnProtected routes
    $api->group(['namespace' => 'App\Http\Controllers'], function ($api) {
        $api->get('test', 'TestController@index');
    });

    // Protected routes
    $api->group(['namespace' => 'App\Http\Controllers','middleware' => 'api.auth'], function ($api) {

        //user routes
        $api->get('users', 'UserController@index');
        $api->post('users', 'UserController@store');
        $api->get('users/{user_id}', 'UserController@show');
        $api->put('users/{user_id}', 'UserController@update');
        $api->delete('users/{user_id}', 'UserController@destroy');
        $api->get('users/{user_id}/{oldPassword}/{newPassword}', 'UserController@changePassword');
        $api->get('user', 'UserController@getCurrentUser');
        $api->get('users/{user_id}/{status}', 'UserController@changeStatus');

        //local purchase
        $api->get('localpurchase','LocalpurchaseController@index');
        $api->get('localpurchase/{date}/{company_id}','LocalpurchaseController@getBydate');
        $api->get('localpurchase/{company_id}','LocalpurchaseController@getbyCompany');
        $api->get('localpurchase/{localpurchase_id}','LocalpurchaseController@show');
        $api->put('localpurchase','LocalpurchaseController@update');
        $api->post('localpurchase','LocalpurchaseController@store');
        $api->delete('localpurchase','LocalpurchaseController@destroy');

        //foreign purchase
        $api->get('foreignpurchase','ForeignPurchaseController@index');
        $api->get('foreignpurchase/{date}/{company_id}','ForeignPurchaseController@getBydate');
        $api->get('foreignpurchase/{company_id}','ForeignPurchaseController@getbyCompany');
        $api->get('foreignpurchase/{foreignpurchase_id}','ForeignPurchaseController@show');
        $api->put('foreignpurchase','ForeignPurchaseController@update');
        $api->post('foreignpurchase','ForeignPurchaseController@store');
        $api->delete('foreignpurchase','ForeignPurchaseController@destroy');

        //local sales
        $api->get('localsales','LocalSalesController@index');
        $api->get('localsales/{date}/{company_id}','LocalSalesController@getBydate');
        $api->get('localsales/{company_id}','LocalSalesController@getbyCompany');
        $api->get('localsalesreport/{date}','LocalSalesController@getlocalSalesReport');
        $api->get('localsales/{localsales_id}','LocalSalesController@show');
        $api->put('localsales','LocalSalesController@update');
        $api->post('localsales','LocalSalesController@store');
        $api->delete('localsales','LocalSalesController@destroy');

        //employeinfo
        $api->get('employeinfo','EmployeInformationController@index');
        $api->get('employeleave/{employe_id}/{date}','EmployeInformationController@getEmplyeLeave');
        $api->get('employeinfo/{employe_id}','EmployeInformationController@show');
        $api->get('employepayroll/{company_id}/{date}','EmployeInformationController@getpayRollreport');
        $api->get('employepension/{company_id}/{date}','EmployeInformationController@getpensionReport');
        $api->get('employebycompany/{company_id}','EmployeInformationController@getBycompany');
        $api->get('employebydate/{company_id}/{date}','EmployeInformationController@getBydate');
        $api->put('employeinfo','EmployeInformationController@update');
        $api->post('employeinfo','EmployeInformationController@store');
        $api->delete('employeinfo/{employe_id}','EmployeInformationController@destroy');
        $api->put('employeinfo/{employe_id}','EmployeInformationController@destroy');
        $api->put('employeupdateall/{company_id}/{date}','EmployeInformationController@updateAllstatus');

        //office Exp
        $api->get('officeexp/{company_id}','OfficeExpenceController@index');
        $api->get('officeexp/{officeexp_id}/{employe_id}','OfficeExpenceController@show');
        $api->put('officeexp/{officeexp_id}','OfficeExpenceController@update');
        $api->post('officeexp','OfficeExpenceController@store');
        $api->delete('officeexp/{officeexp_id}','OfficeExpenceController@destroy');


        //vat info
        $api->get('vatinfo/{vatinfo_id}/{date}','VatInfoController@getvatInfobydate');
        $api->put('vatinfo/{vatinfo_id}','VatInfoController@update');
        $api->post('vatinfo','VatInfoController@store');
        $api->delete('vatinfo/{vatinfo_id}','VatInfoController@destroy');

        //overtime
        $api->get('overtime/{company_id}','OvertimeController@index');
        $api->get('overtime/{overtime_id}','OvertimeController@show');
        $api->get('overtimebyemp/{employe_id}','OvertimeController@showByemp');
        $api->get('overtimebycompany/{company_id}/{date}','OvertimeController@showByCompany');
        $api->put('overtime','OvertimeController@update');
        $api->post('overtime','OvertimeController@store');
        $api->delete('overtime/{overtime_id}','OvertimeController@destroy');

         //enchashed leave
        $api->get('leave/{company_id}','EncashedLeaveController@index');
        $api->get('fixedleavebycompany/{company_id}/{date}','EncashedLeaveController@getfixedleaveBycompany');
        $api->get('variableleavebycompany/{company_id}/{date}','EncashedLeaveController@getvariableleaveBycompany');
        $api->get('leave/{leave_id}/{company_id}','EncashedLeaveController@show');
        $api->put('leave','EncashedLeaveController@update');
        $api->post('leave','EncashedLeaveController@store');
        $api->delete('leave/{leave_id}','EncashedLeaveController@destroy');


        //termination
        $api->get('termination/{company_id}','TerminationController@index');
        $api->get('serverancepay/{company_id}/{date}','TerminationController@getservrancepayBYcompany');
        $api->get('termination/{termination_id}','TerminationController@show');

        $api->put('termination','TerminationController@update');
        $api->post('termination','TerminationController@store');
        $api->delete('termination/{termination_id}','TerminationController@destroy');

        //allowance
        $api->get('allowanceall/{company_id}','AllowanceController@index');
        $api->get('allowance/{allowance_id}','AllowanceController@show');
        $api->get('allowancebydate/{company_id}/{date}','AllowanceController@getBycompanybydate');
        $api->get('allowancebycompany/{company_id}','AllowanceController@getBycompany');
        $api->get('allowance/{company_id}/{employe_id}','AllowanceController@getByemploye');
        $api->put('allowance','AllowanceController@update');
        $api->post('allowance','AllowanceController@store');
        $api->delete('allowance/{allowance_id}','AllowanceController@destroy');


        //bonus
        $api->get('bonusall/{company_id}','BonusController@index');
        $api->get('bonus/{bonus_id}','BonusController@show');
        $api->get('bonusfixedbycompany/{company_id}/{date}','BonusController@getfixedBycompany');
        $api->get('bonusvariablebycompany/{company_id}/{date}','BonusController@getvariableBycompany');
        $api->put('bonusupdate','BonusController@update');
        $api->post('bonusstore','BonusController@store');
        $api->delete('bonus/{allowance_id}','BonusController@destroy');

        //non-registered purchase

        $api->get('nonregpurchase','NonRegisteredPurchaseController@index');
        $api->get('nonregpurchase/{date}/{company_id}','NonRegisteredPurchaseController@getBydate');
        $api->get('nonregpurchase/{company_id}','NonRegisteredPurchaseController@getbyCompany');
        $api->get('nonregpurchase/{nonregpurchase_id}','NonRegisteredPurchaseController@show');
        $api->put('nonregpurchase','NonRegisteredPurchaseController@update');
        $api->post('nonregpurchase','NonRegisteredPurchaseController@store');
        $api->delete('nonregpurchase','NonRegisteredPurchaseController@destroy');

        //vat report
        $api->get('vat_report/{date}/{company_id}','VATReportController@getvatReport');
        $api->get('withhold_report/{date}/{company_id}','VATReportController@getwithholdingReport');
        $api->put('updatstatus/{date}/{company_id}','VATReportController@upadateAll');

  });
}
);
