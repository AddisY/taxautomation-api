<?php

namespace App\Http\Controllers;

use App\Entities\ForeignPurchases;
use App\Repositories\ForeignPurchasesRepository;
use App\Transformers\ForeignPurchasesTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ForeignPurchaseController extends Controller
{
    use Helpers;
    /**
     * LocalPurchaseController constructor.
     */
    protected $repositoryforeignPurchase;
    protected  $per_page=10;

    /**
     * ForeignPurchaseController constructor.
     * @param $repositoryforeignPurchase
     */

    public function __construct(ForeignPurchasesRepository $repositoryforeignPurchase)
    {
        $this->repositoryforeignPurchase = $repositoryforeignPurchase;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function getBydate(Request $request,$date,$uid){
        if($request->get('all')==='true'){
            $foreignPurchase=$this->repositoryforeignPurchase->getforeignpurchaseBydate($date,$uid)->get();
            $response= $this->response->collection($foreignPurchase,new ForeignPurchasesTransformer());
        }else{
            $foreignPurchase=$this->repositoryforeignPurchase->getforeignpurchaseBydate($date,$uid)->paginate($this->per_page);
            $response= $this->response->paginator($foreignPurchase,new ForeignPurchasesTransformer());
        }
        return $response;


    }
    public function index()
    {
        $foreignPurchase= $this->repositoryforeignPurchase->getallforeignPurchase($this->per_page);
        $response=$this->response->paginator($foreignPurchase,new ForeignPurchasesTransformer());
        return $response;

    }

    public function getbyCompany($company_id){
        $foreginPurchas =$this->repositoryforeignPurchase->getbyCompany($company_id);
        $response=$this->response->collection($foreginPurchas,new ForeignPurchasesTransformer());
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $foreignPurchase= $this->repositoryforeignPurchase->storeforeignPurchase($request);

        return $foreignPurchase;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $foreignPurchase= $this->repositoryforeignPurchase->getforeignPurchase($id);
        $response=$this->response->collection($foreignPurchase,new ForeignPurchasesTransformer());
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $foreignPurchase= $this->repositoryforeignPurchase->updateforeignPurchase($request);
        return $foreignPurchase;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $ids)
    {
        $foreignPurchase= $this->repositoryforeignPurchase->deleteforeignPurchase($ids);
        return $foreignPurchase;
    }
}
