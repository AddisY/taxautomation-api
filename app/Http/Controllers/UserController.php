<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use App\Transformers\UserTransformerDropDown;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{
    use Helpers;

    protected $per_page = 10;
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        if ($request->has('dropdown')) {
            $users = $this->userRepository->getAllUsers($request,true,$this->per_page);
            $response = $this->response->collection($users, new UserTransformerDropDown());
        } else {
            $users = $this->userRepository->getAllUsers($request,false,$this->per_page);
            $response = $this->response->paginator($users, new UserTransformer());
        }

        return $response;
    }

    public function store(Request $request)
    {
        $user = $this->userRepository->storeUser($request);

        $response = $this->response->item($user, new UserTransformer());

        return $response;
    }

    public function show($id)
    {
        $user = $this->userRepository->getUserDetail($id);

        $response = $this->response->item($user, new UserTransformer());

        return $response;
    }

    public function update(Request $request, $userId)
    {
        $user = $this->userRepository->updateUser($request, $userId);

        $response = $this->response->item($user, new UserTransformer());

        return $response;

    }

    public function destroy($id)
    {
        $user = $this->userRepository->destroyUser($id);

        $response = $this->response->item($user, new UserTransformer());

        return $response;
    }

    public function changeStatus($user_id,$status)
    {
        $user = $this->userRepository->changeStatus($user_id,$status);

        $response = $this->response->item($user, new UserTransformer());

        return $response;
    }

    public function  getCurrentUser()
    {
        $user = $this->userRepository->getCurrentUser();

        $response = $this->response->item($user, new UserTransformer());

        return $response;
    }

    public function changePassword($user_id, $oldPassword, $newPassword)
    {
        return $this->userRepository->changePassword($user_id, $oldPassword, $newPassword);
    }

}
