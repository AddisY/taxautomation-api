<?php

namespace App\Http\Controllers;

use App\Entities\LocalSales;
use App\Repositories\LocalPurchaseRepository;
use App\Repositories\LocalSalesRepository;
use App\Transformers\LocalPurchaseTransformer;
use App\Transformers\VATReportTransformer;
use App\Transformers\LocalSalesTransformer;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LocalSalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use Helpers;
    protected $localsalesRepository;
    protected $per_page=10;

    /**
     * LocalSalesController constructor.
     * @param $localsalesRepository
     */
    public function __construct(LocalSalesRepository $localsalesRepository)
    {
        $this->localsalesRepository = $localsalesRepository;
    }

    public function index()
    {
        $localSales= $this->localsalesRepository->getalllocalSales($this->per_page);
        $response=$this->response->paginator($localSales,new LocalSalesTransformer());
        return $response;

    }
    public function getlocalSalesReport($date){
         $localSales= $this->localsalesRepository->getlocalSalesSummery($date);
        $response=$this->response->item($localSales,new VATReportTransformer());
        return $response;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


    }
    public function getBydate(Request $request,$date,$uid){
        if($request->get('all')==='true'){
            $localSalses=$this->localsalesRepository->getlocalsalesBydate($date,$uid)->get();
            $response=$this->response->collection($localSalses,new LocalSalesTransformer());

        }else{

            $localSalses=$this->localsalesRepository->getlocalsalesBydate($date,$uid)->paginate($this->per_page);
            $response=$this->response->paginator($localSalses,new LocalSalesTransformer());


        }

        return $response;
    }
    public function getbyCompany($company_id){
        $localsales=$this->localsalesRepository->getbycompany($company_id);
        $response= $this->response->collection($localsales,new LocalPurchaseTransformer());

        return $localsales;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $localSales= $this->localsalesRepository->storelocalSales($request);

        return $localSales;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $localSales= $this->localsalesRepository->getlocalSales($id);
        $response=$this->response->collection($localSales,new LocalSalesTransformer());
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $localSales= $this->localsalesRepository->updatelocalSales($request);

        return $localSales;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $ids)
    {
        $localSales= $this->localsalesRepository->deletelocalSales($ids);

       return $localSales;

    }
}
