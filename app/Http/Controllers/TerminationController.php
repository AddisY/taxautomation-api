<?php

namespace App\Http\Controllers;

use App\Repositories\TerminationRepository;
use App\Transformers\TerminationTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TerminationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use Helpers;
    protected $terminationRepository;
    protected $per_page=10;

    /**
     * TerminationController constructor.
     * @param $terminationRepository
     */
    public function __construct(TerminationRepository $terminationRepository)
    {
        $this->terminationRepository = $terminationRepository;
    }

    public function index($company_id)
    {
        $termination = $this->terminationRepository->getAlltermination($company_id);
       // $response= $this->response->paginator($termination,new TerminationTransformer());
        return $termination;


    }
    public function getservrancepayBYcompany($company_id,$date){
        $serverancepay=$this->terminationRepository->getseverancePaybycompany($company_id,$date);
        return $serverancepay;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $termination = $this->terminationRepository->storeTermination($request);
        return $termination;
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $termination = $this->terminationRepository->getTermination($id);
        $response= $this->response->collection($termination,new TerminationTransformer());
    return $response;


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $termination = $this->terminationRepository->updateTermination($request);
        $response= $this->response->item($termination,new TerminationTransformer());
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $termination = $this->terminationRepository->deleteTermination($id);
        $response= $this->response->item($termination,new TerminationTransformer());
        return $response;
    }
}
