<?php

namespace App\Http\Controllers;

use App\Repositories\VATReportRepository;
use App\Transformers\VATReportTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VATReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use Helpers;
    protected $vatreportRepository;

    /**
     * VATReportController constructor.
     * @param $vatreportRepository
     */
    public function __construct(VATReportRepository $vatreportRepository)
    {
        $this->vatreportRepository = $vatreportRepository;
    }


    public function getvatReport($date,$company_id){
        $vat_report =$this->vatreportRepository->getvatReport($date,$company_id);
        $response=$this->response->item($vat_report,new VATReportTransformer());
        return $response;
    }
    public function getwithholdingReport($date,$company_id){
       $withholding_report=$this->vatreportRepository->getwithholdinglocalpurchaseReport($date,$company_id);
        return $withholding_report;
}


    public function upadateAll($date,$company_id){
        $transaction=$this->vatreportRepository->updateAllstatus($date,$company_id);
            return $transaction;
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
