<?php

namespace App\Http\Controllers;

use App\Entities\NonRegisteredPurchase;
use App\Repositories\NonRegisteredPurchaseRepository;
use App\Transformers\NonRegisteredPurchaseTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NonRegisteredPurchaseController extends Controller
{
    /**
     * NonRegisteredPurchaseController constructor.
     */
    use Helpers;
    protected $nonregisteredPurchaseRepository;
    protected $per_page;
    public function __construct(NonRegisteredPurchaseRepository $nonregisteredPurchaseRepository)
    {
        $this->nonregisteredPurchaseRepository=$nonregisteredPurchaseRepository;
    }
    public function index()
    {
        $nonregPurchase= $this->nonregisteredPurchaseRepository->getallnonregisteredPurchases($this->per_page);
        $response=$this->response->paginator($nonregPurchase,new NonRegisteredPurchaseTransformer());
        return $response;

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


    }


    public  function  getbyCompany($company_id){
       $nonregPurchase=$this->nonregisteredPurchaseRepository->getbyCompany($company_id);
        $respons= $this->response->collection($nonregPurchase,new NonRegisteredPurchaseTransformer());
        return $respons;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nonregPurchase= $this->nonregisteredPurchaseRepository->storenonregisteredPurchase($request);
        return $nonregPurchase;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nonregPurchase= $this->nonregisteredPurchaseRepository->getnonregisteredPurchases($id);
        $response=$this->response->collection($nonregPurchase,new NonRegisteredPurchaseTransformer());
        return $response;
    }
    public function getBydate(Request $request,$date,$uid){
        if($request->get('all')==='true'){
            $nonregPurchase=$this->nonregisteredPurchaseRepository->getnonregisteredBydate($date,$uid)->get();
            $response=$this->response->collection($nonregPurchase,new NonRegisteredPurchaseTransformer());
        }else{
            $nonregPurchase=$this->nonregisteredPurchaseRepository->getnonregisteredBydate($date,$uid)->paginate($this->per_page);
            $response=$this->response->paginator($nonregPurchase,new NonRegisteredPurchaseTransformer());

        }

        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $nonregPurchase = $this->nonregisteredPurchaseRepository->updatenonregisteredPurchase($request);

        return $nonregPurchase;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        $nonregPurchase= $this->nonregisteredPurchaseRepository->deletenonregisteredPurchase($ids);
        return $nonregPurchase;
    }
}
