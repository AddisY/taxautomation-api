<?php

namespace App\Http\Controllers;

use App\Repositories\LocalPurchaseRepository;
use App\Transformers\LocalPurchaseTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\Console\Helper\Helper;

class LocalpurchaseController extends Controller
{

     use Helpers;
    /**
     * LocalPurchaseController constructor.
     */
    protected $repositorylocalPurchase;
    protected  $per_page=10;
    public function __construct(LocalPurchaseRepository $repositorylocalPurchase)
    {

        $this->repositorylocalPurchase=$repositorylocalPurchase;
    }

    public function getBydate(Request $request,$date,$uid){
        if($request->get('all')==='true'){
            $localPurchase= $this->repositorylocalPurchase->getlocalpurchaseBydate($date,$uid)->get();
            $response= $this->response->collection($localPurchase,new LocalPurchaseTransformer());
        }else{
            $localPurchase= $this->repositorylocalPurchase->getlocalpurchaseBydate($date,$uid)->paginate($this->per_page);
            $response= $this->response->paginator($localPurchase,new LocalPurchaseTransformer());
        }

        return $response;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $localPurchase= $this->repositorylocalPurchase->getAlllocalpurchase($this->per_page);
        $response=$this->response->paginator($localPurchase,new LocalPurchaseTransformer());
        return $response;

    }
    public function getbyCompany($company_id){
        $localpurchase=$this->repositorylocalPurchase->getbycompany($company_id);
        $response=$this->response->collection($localpurchase,new LocalPurchaseTransformer());
        return $response;
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $localPurchase= $this->repositorylocalPurchase->storelocalPurchase($request);

        return $localPurchase;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $localPurchase= $this->repositorylocalPurchase->getlocalPurchase($id);
        $response=$this->response->collection($localPurchase,new LocalPurchaseTransformer());
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $localPurchase= $this->repositorylocalPurchase->updatelocalPurchase($request);

        return $localPurchase;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $ids)
    {
        $localPurchase= $this->repositorylocalPurchase->deletelocalPurchase($ids);
       return $localPurchase;

    }

}
