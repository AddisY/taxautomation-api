<?php

namespace App\Http\Controllers;

use App\Repositories\OfficeExpensesRepository;
use App\Transformers\OfficeExpensesTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OfficeExpenceController extends Controller
{

   use Helpers;
    protected $officeexpRepository;
    protected $per_page;
    /**
     * OfficeExpenceController constructor.
     * @param $officeexpRepository
     */
    public function __construct(OfficeExpensesRepository $officeexpRepository)
    {
        $this->officeexpRepository = $officeexpRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company_id)
    {
        //
        $officeExp=$this->officeexpRepository->getAllofficeexp($company_id);
        $response=$this->response->item($officeExp,new OfficeExpensesTransformer());
        return $response;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $officeExp=$this->officeexpRepository->storeOfficeexp($request);
        $response= $this->response->item($officeExp,new OfficeExpensesTransformer());
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$company_id)
    {
        $officeExp=$this->officeexpRepository->getofficeexp($id,$company_id);
        $response=$this->response->collection($officeExp,new OfficeExpensesTransformer());
        return $response;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $officeExp=$this->officeexpRepository->updateOfficeExp($request,$id);
        $response=$this->response->item($officeExp,new OfficeExpensesTransformer());
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $officeExp=$this->officeexpRepository->deleteOfficeexp($id);
        $response= $this->response->item($officeExp,new OfficeExpensesTransformer());

        return $response;
    }
}
