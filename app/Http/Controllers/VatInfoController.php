<?php

namespace App\Http\Controllers;

use App\Repositories\VatInfoRepository;
use App\Repositories\VATReportRepository;
use App\Transformers\VatInfoTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VatInfoController extends Controller
{
    use Helpers;
    protected $vatInfoRepository;
    protected $per_page;
    /**
     * OfficeExpenceController constructor.
     * @param $vatInfoRepository
     */
    public function __construct(VatInfoRepository $vatInfoRepository)
    {
        $this->vatInfoRepository = $vatInfoRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getvatInfobydate($company_id,$date)
    {

        $vatInfo=$this->vatInfoRepository->getVatinfoBydate($company_id,$date);
        $response=$this->response->collection($vatInfo,new VatInfoTransformer());
        return $response;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $vatInfo=$this->vatInfoRepository->storevatInfo($request);
        $response= $this->response->item($vatInfo,new VatInfoTransformer());
        return $response;
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vatInfo=$this->vatInfoRepository->updatevatInfo($request,$id);
        $response=$this->response->item($vatInfo,new VatInfoTransformer());
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $vatInfo=$this->vatInfoRepository->deletevatInfo($id);
        $response= $this->response->item($vatInfo,new VatInfoTransformer());

        return $response;
    }
}
