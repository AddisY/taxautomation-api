<?php

namespace App\Http\Controllers;

use App\Repositories\AllowanceRepository;
use App\Transformers\AllowanceTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AllowanceController extends Controller
{
    use Helpers;
    protected $allowanceRepository;
     protected $per_page=10;

    /**
     * AllowanceController constructor.
     * @param $allowanceRepository
     */
    public function __construct(AllowanceRepository $allowanceRepository)
    {
        $this->allowanceRepository = $allowanceRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company_id)
    {
        $allowance=$this->allowanceRepository->getAllallowance($company_id);
        //$response=$this->response->paginator($allowance,new AllowanceTransformer());
        return $allowance;
    }

    public function getBycompany($company_id){
        $allowance = $this->allowanceRepository->getAllowanceBycompany($company_id);
        return $allowance;
    }

    public function getBycompanybydate($company_id,$date){
        $allowance = $this->allowanceRepository->getAllowanceBydate($company_id,$date);
        return $allowance;
    }


    public function getByemploye($company_id,$employe_id){
        $allowance = $this->allowanceRepository->getallowanceByEmploye($company_id,$employe_id);
        $response = $this->response->collection($allowance,new AllowanceTransformer());
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $allowance = $this->allowanceRepository->storeAllowance($request);
        return $allowance;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $allowance = $this->allowanceRepository->getAllowance($id);
        $response= $this->response->item($allowance,new AllowanceTransformer());
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $allowance=$this->allowanceRepository->updateAllowance($request);

        return $allowance;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $allowance=$this->allowanceRepository->deleteAllowance($id);
        $response=$this->response->item($allowance,new AllowanceTransformer());
        return $response;
    }
}
