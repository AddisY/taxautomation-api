<?php

namespace App\Http\Controllers;

use App\Repositories\EncashedLeaveRepository;
use App\Transformers\EncashedLeaveTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EncashedLeaveController extends Controller
{

   use Helpers;
    protected $leaveRepository;
    protected $per_page=10;

    /**
     * EncashedLeaveController constructor.
     * @param $leaveRepository
     */
    public function __construct(EncashedLeaveRepository $leaveRepository)
    {
        $this->leaveRepository = $leaveRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company_id)
    {
        $leave =$this->leaveRepository->getAllLeave($company_id);
       // $response= $this->response->paginator($leave,new EncashedLeaveTransformer());
        return $leave;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $leave = $this->leaveRepository->storeLeave($request);
        return $leave;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$employe_id)
    {
        $leave=$this->leaveRepository->getLeave($id,$employe_id);
        $response=$this->response->collection($leave,new EncashedLeaveTransformer());
        return $response;
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function getfixedleaveBycompany($company_id,$date){
        $leave=$this->leaveRepository->getfixedEncashedBycompany($company_id,$date);
        return $leave;

    }
    public  function getvariableleaveBycompany($company_id,$date){
        $leave=$this->leaveRepository->getvariableEncashedBycompany($company_id,$date);
        return $leave;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $leave=$this->leaveRepository->updateLeave($request);

        return $leave;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $leave = $this->leaveRepository->deleteLeave($id);

        $response= $this->response->item($leave,new EncashedLeaveTransformer());
        return $response;
    }
}
