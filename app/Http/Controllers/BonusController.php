<?php

namespace App\Http\Controllers;

use App\Entities\Bonus;
use App\Repositories\BonusRepository;
use App\Transformers\BonusTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BonusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use Helpers;
    protected $bonusRepository;
    protected $per_page=10;

    /**
     * BonusController constructor.
     * @param $bonusRepository
     */
    public function __construct(BonusRepository $bonusRepository)
    {
        $this->bonusRepository = $bonusRepository;
    }

    public function index($company_id)
    {
        return $bonus=$this->bonusRepository->getAllbonus($company_id);
        $response = $this->response->item($bonus, new BonusTransformer());
        return $response;
    }
    public function getfixedBycompany($company_id,$date){
        $bonus = $this->bonusRepository->getfixedBonusBycompany($company_id,$date);
        return $bonus;
    }
    public function getvariableBycompany($company_id,$date){
        $bonus =$this->bonusRepository->getvariableBonusbycompany($company_id,$date);
        return $bonus;

}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $bonus= $this->bonusRepository->storeBonus($request);
        return $bonus;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bonus=$this->bonusRepository->getBonus($id);
        $response = $this->response->item($bonus,new BonusTransformer());
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $bonus= $this->bonusRepository->updateBonus($request);
        return $bonus;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bonus= $this->bonusRepository->deleteBonus($id);
        $response= $this->response->item($bonus,new BonusTransformer());
        return $response;
    }
}
