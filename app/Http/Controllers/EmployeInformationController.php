<?php

namespace App\Http\Controllers;

use App\Repositories\EmployeInformationRepository;
use App\Transformers\EmployeInformationTransformer;
use App\Transformers\SalaryTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmployeInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     use Helpers;
    protected $employeinfoRepository;
    protected $per_page=10;

    /**
     * EmployeInformationController constructor.
     * @param $employeinfoRepository
     */
    public function __construct(EmployeInformationRepository $employeinfoRepository)
    {
        $this->employeinfoRepository = $employeinfoRepository;
    }


    public function index()
    {
        $employeInfo=$this->employeinfoRepository->getallEmploye($this->per_page);
        $response = $this->response->paginator($employeInfo,new EmployeInformationTransformer());
        return $response;
    }

    public function getBydate(Request $request,$company_id,$date){
        if($request->get('all')==='true'){
            $employeInfo= $this->employeinfoRepository->getEmployebydate($company_id,$date)->get();
            $response= $this->response->collection($employeInfo,new EmployeInformationTransformer());
        }else{
            $employeInfo= $this->employeinfoRepository->getEmployebydate($company_id,$date)->paginate($this->per_page);
            $response= $this->response->paginator($employeInfo,new EmployeInformationTransformer());
        }

        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getEmplyeLeave($id,$date){
        $employeinfo=$this->employeinfoRepository->getemployeLeave($id,$date);
        return $employeinfo;
    }
    public function store(Request $request)
    {
        $employeinfo=$this->employeinfoRepository->storeEmployeInfo($request);
        return $employeinfo;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employeinfo = $this->employeinfoRepository->getemployeeInfo($id);
        $response=$this->response->item($employeinfo,new EmployeInformationTransformer());
        return $response;
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public  function updateAllstatus($company_id,$date){
        $declaration =$this->employeinfoRepository->updatestatus($company_id,$date);
        return $declaration;

    }
    public  function getBycompany($company_id){
        $employe = $this->employeinfoRepository->getEmployeBycompany($company_id);
        return $employe;
    }


    public function getpayRollreport($company_id,$date){
        $company=$this->employeinfoRepository->getpayrollReport($company_id,$date);
        return $company;
    }

    function getpensionReport($company_id,$date){
        $company=$this->employeinfoRepository->getpensionReport($company_id,$date);
        return $company;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $employeinfo = $this->employeinfoRepository->updateEmployeInfo($request);
        return $employeinfo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employe=$this->employeinfoRepository->deleteEmploye($id);
        $response =$this->response->item($employe,new EmployeInformationTransformer());
        return $response;
    }
    public function updateSalary(Request $request,$id){
        $employeSalary=$this->employeinfoRepository->updateSalary($request,$id);
        $response=$this->response->item($employeSalary,new SalaryTransformer());
    }
}
