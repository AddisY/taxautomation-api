<?php

namespace App\Http\Controllers;

use App\Entities\EmployeInformation;
use App\Entities\OverTime;
use App\Repositories\OverTimeRepository;
use App\Transformers\EmployeInformationTransformer;
use App\Transformers\OverTimeTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OvertimeController extends Controller
{
    use Helpers;
    protected $overtimeRepository;
    protected $per_page=10;

    /**
     * OvertimeController constructor.
     * @param $overtimeRepository
     */
    public function __construct(OverTimeRepository $overtimeRepository)
    {
        $this->overtimeRepository = $overtimeRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company_id)
    {
         $overTime=$this->overtimeRepository->getAllovertime($company_id);
       // $response=$this->response->item($overTime,new OverTimeTransformer());
        return $overTime;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $overTime = $this->overtimeRepository->storeOverTime($request);
        return $overTime;
    }


    public function showByemp($employe_id)
    {
        $overTime=$this->overtimeRepository->getOvertimeByemployeID($employe_id);
        $response= $this->response->collection($overTime,new OverTimeTransformer());
        return $response;
    }
    public function showbyCompany($company_id,$date){
        $overtime = $this->overtimeRepository->getovertimeBycompanydate($company_id,$date);
        //$response = $this->response->item($overtime,new EmployeInformationTransformer());
        return $overtime;
    }
    public function getbyCompany($company_id){
        $overtime= $this->overtimeRepository->getbyCompany($company_id);
        return $overtime;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $overTime = $this->overtimeRepository->getOvertime($id);
        $response=$this->response->item($overTime,new OverTimeTransformer());
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $overTime=$this->overtimeRepository->updateOvertime($request);
        $response=$this->response->item($overTime,new OverTimeTransformer());
        return $response;
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $overTime=$this->overtimeRepository->deleteOverTime($id);
        $response=$this->response->item($overTime,new OverTimeTransformer());
        return $response;
    }
}
