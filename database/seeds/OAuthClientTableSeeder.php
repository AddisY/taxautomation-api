<?php

use App\Entities\Auth\Client;
use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class OAuthClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Client::create([
           'id' => '$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD',
           'secret' => '$2y$10$9OqJjxC9qZKAB2L.nO7hVOPY0436eU1CD',
           'name' => 'Web Client',
       ]);
    }
}
