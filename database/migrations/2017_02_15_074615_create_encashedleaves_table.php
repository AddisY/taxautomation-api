<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEncashedleavesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('encashed_leaves', function(Blueprint $table) {
            $table->string('id',50)->primary()->unique();
			$table->string('employe_id');
			$table->foreign('employe_id')->references('id')
				->on('employe_informations')
				->onUpdate('cascade')
				->onDelete('cascade');
           $table->integer('leave_date');
           $table->integer('working_date');//per month
			$table->integer('annual_leave');
			$table->date('issued_date');
			$table->integer('status');
			$table->string('served_leaved_period');
			$table->string('leave_accumulated');
			$table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('encashed_leaves');
	}

}
