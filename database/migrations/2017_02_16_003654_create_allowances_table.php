<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllowancesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('allowances', function(Blueprint $table) {
			$table->string('id',50)->primary()->unique();
			$table->string('employe_id');
			$table->foreign('employe_id')->references('id')
				->on('employe_informations')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->string('type');
			$table->string('description');
			$table->double('total_amount');
			$table->date('start');
			$table->date('end');
			$table->integer('status');
			$table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations./
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('allowances');
	}

}
