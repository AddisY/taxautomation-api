
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeinformationsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employe_informations', function(Blueprint $table) {
			$table->string('id',50)->primary()->unique();
			$table->string('full_name');
			$table->unsignedInteger('company_id');
			$table->foreign('company_id')->references('uid')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->string('tin_number');
			$table->string('department');
			$table->integer('status');
			$table->date('start_date');
			$table->double('cost_sharing');
		    $table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employe_informations');
	}

}
