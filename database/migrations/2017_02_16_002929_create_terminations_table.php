<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminationsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('terminations', function(Blueprint $table) {
            $table->string('id',50)->primary()->unique();
			$table->string('employe_id');
			$table->foreign('employe_id')->references('id')
				->on('employe_informations')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->date('end_date');
			$table->integer('status');
			$table->integer('type');
			$table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('terminations');
	}

}
