<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVatinfosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vatinfos', function(Blueprint $table) {
			$table->string('id',50)->primary()->unique();
			$table->unsignedInteger('company_id');
			$table->foreign('company_id')->references('uid')->
			on('users')->onUpdate('cascade')->onDelete('cascade');
			$table->double('value_zero_goods');
			$table->double('exempted_supply');
			$table->double('vat_returened_supplies');
			$table->date('date');
			$table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vatinfos');
	}

}
