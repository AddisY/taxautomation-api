<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foreign_purchases', function (Blueprint $table) {
            $table->string('id',50)->primary()->unique();
            $table->string('dec_number');
            $table->unsignedInteger('company_id');
            $table->foreign('company_id')->references('uid')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->date('dec_date');
            $table->string('bill_code');
            $table->double('tax_price');
            $table->double('item_price');
            $table->integer('status');
            $table->string('item_name');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('foreign_purchases');
    }
}
