<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonRegisteredPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nonregistered_purchases', function (Blueprint $table) {
            $table->string('id',50)->primary()->unique();
            $table->string('company_name');
            $table->unsignedInteger('company_id');
            $table->foreign('company_id')->references('uid')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->string('zone');
            $table->string('sub_city');
            $table->string('woreda');
            $table->string('house_number');
            $table->string('purchase_type');
            $table->double('item_price');
            $table->integer('status');
            $table->string('voucher_number');
            $table->date('voucher_date');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nonregistered_purchases');
    }
}
