<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_purchases', function (Blueprint $table) {
            $table->string('id',50)->primary()->unique();
            $table->string('company_name');
            $table->unsignedInteger('company_id');
            $table->double('item_price');
            $table->string('tin_number');
            $table->string('item_type');
            $table->string('withholding_tax_number');
            $table->string('purchase_type');
            $table->string('mrc_code');
            $table->string('bill_code');
            $table->string('item_name');
            $table->integer('status');
            $table->date('bill_date');
            $table->foreign('company_id')->references('uid')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->date('withholding_date');
            $table->string('withholding_code');
           $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('local_purchases');
    }
}
