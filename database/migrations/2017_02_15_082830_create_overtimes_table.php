<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOvertimesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('overtimes', function(Blueprint $table) {
            $table->string('id',50)->primary()->unique();
			$table->string('employe_id');
			$table->foreign('employe_id')->references('id')
				->on('employe_informations')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->date('overtime_date');
			$table->integer('status');
			$table->string('start_time');
			$table->string('end_time');
			$table->string('remark');
			$table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('over_times');
	}

}
