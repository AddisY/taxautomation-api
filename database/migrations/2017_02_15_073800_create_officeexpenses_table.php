<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficeexpensesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('office_expenses', function(Blueprint $table) {
            $table->string('id',50)->primary()->unique();
			$table->date('issue_date');
			$table->string('bill_code');
			$table->double('total_price');
			$table->unsignedInteger('company_id');
			$table->integer('status');
			$table->foreign('company_id')->references('uid')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('office_expenses');
	}

}
