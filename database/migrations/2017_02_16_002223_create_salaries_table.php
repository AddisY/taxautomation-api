<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('salaries', function(Blueprint $table) {
            $table->string('id',50)->primary()->unique();
			$table->double('total_salary');
			$table->string('employe_id');
			$table->foreign('employe_id')->references('id')
				->on('employe_informations')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->string('payment_type');
			$table->integer('status');
			$table->string('duration');
			$table->date('pay_day');
			$table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('salaries');
	}

}
