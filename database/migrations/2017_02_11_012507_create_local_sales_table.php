<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_sales', function (Blueprint $table) {
            $table->string('id',50)->primary()->unique();
            $table->unsignedInteger('company_id');
            $table->foreign('company_id')->references('uid')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->string('item_name');
            $table->string('item_price');
            $table->string('bill_code');
            $table->integer('status');
            $table->date('bill_date');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('local_salses');
    }
}
