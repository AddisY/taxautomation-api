<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bonuses', function(Blueprint $table) {
			$table->string('id',50)->primary()->unique();
			$table->string('employe_id');
			$table->foreign('employe_id')->references('id')
				->on('employe_informations')
				->onUpdate('cascade')
				->onDelete('cascade');
		    $table->string('total_amount');
		    $table->string('served_leaved_period');
			$table->date('pay_day');
			$table->integer('status');
			$table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bonuses');
	}

}
