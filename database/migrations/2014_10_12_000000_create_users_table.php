<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('uid')->comment('Primary Key: Unique user ID.');
            $table->string('name', 60)->default('')->unique('name')->comment('Unique user name.');
            $table->string('pass', 128)->default('')->comment('User�s password (hashed).');
            $table->string('mail', 254)->nullable()->default('')->index('mail')->comment('User�s e-mail address.');
            $table->string('theme')->default('')->comment('User�s default theme.');
            $table->string('signature')->default('')->comment('User�s signature.');
            $table->string('signature_format')->nullable()->comment('The filter_format.format of the signature.');
            $table->integer('created')->default(0)->index('created')->comment('Timestamp for when user was created.');
            $table->integer('access')->default(0)->index('access')->comment('Timestamp for previous time user accessed the site.');
            $table->integer('login')->default(0)->comment('Timestamp for user�s last login.');
            $table->boolean('status')->default(0)->comment('Whether the user is active(1) or blocked(0).');
            $table->string('timezone', 32)->nullable()->comment('User�s time zone.');
            $table->string('language', 12)->default('')->comment('User�s default language.');
            $table->integer('picture')->default(0)->index('picture')->comment('Foreign key: file_managed.fid of user�s picture.');
            $table->string('init', 254)->nullable()->default('')->comment('E-mail address used for initial account creation.');
            $table->binary('data')->nullable()->comment('A serialized array of name value pairs that are related to the user. Any form values posted during user edit are stored and are loaded into the $user object during user_load(). Use of this field is discouraged and it will likely disappear in a future...');
            $table->string('first_name', 60)->nullable();
            $table->string('middle_name', 60)->nullable();
            $table->string('last_name', 60)->nullable();
            $table->integer('line_number')->nullable();
            $table->integer('mobile_number')->nullable();
            $table->integer('tin_number')->nullable();
            $table->string('decoy', 60)->nullable();
            $table->integer('package');
            $table->integer('payment')->nullable();
            $table->string('cheque_no', 60)->nullable();
            $table->text('bank', 65535)->nullable();
            $table->text('branch', 65535)->nullable();
            $table->integer('type')->nullable();
            $table->boolean('construction')->nullable();
            $table->boolean('industry')->nullable();
            $table->boolean('service')->nullable();
            $table->boolean('ngo')->nullable();
            $table->boolean('agriculture')->nullable();
            $table->boolean('bank_insurance')->nullable();
            $table->boolean('import_export')->nullable();
            $table->boolean('other_sectors')->nullable();
            $table->boolean('business_entity')->nullable();
            $table->string('country', 60);
            $table->string('city', 60);
            $table->string('sub_city', 60);
            $table->string('province', 60);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('users');
    }
}
